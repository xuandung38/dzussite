<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Models\SongList;
use App\Models\MusicAlbum;
use App\Models\Artists;

class SongListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $songs = SongList::with('album')->paginate(50);        
        return view('admin.songs.list',compact('songs')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $albums = MusicAlbum::all();
        $artists = Artists::all();
        return view('admin.songs.create',compact('albums', 'artists')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $song =  new SongList();
        $song->title = $request->input('title');
        $song->slug = Str::slug($request->input('title'),'-');
        $song->images = $request->input('images');
        $song->descriptions = $request->input('descriptions');
        $song->lyrics = $request->input('lyrics');
        $song->tags = $request->input('tags');
        $song->download_link = $request->input('download_link');
        $song->album_id = $request->input('album_id');
        $song->youtube_id = $request->input('youtube_id');
        $song->save();
        $song->artists()->sync($request->get('artist_id'), false);        
        return redirect()->route('be.song')->with(['statuss' => 'success','message' => 'Thêm Bài Hát thành công']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $albums = MusicAlbum::all();
        $song = SongList::with('artists')->findorFail($id);
        $artists = Artists::all();
        return view('admin.songs.edit', compact('song', 'albums', 'artists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $song = SongList::findorFail($id);
        $song->update([
            'title'         => $request->input('title'),
            'slug'          => Str::slug($request->input('title'),'-'),
            'images'        => $request->input('images'),
            'descriptions'  => $request->input('descriptions'),
            'lyrics'        => $request->input('lyrics'),
            'tags'          => $request->input('tags'),
            'download_link' => $request->input('download_link'),
            'album_id'      => $request->input('album_id'),
            'youtube_id'    => $request->input('youtube_id')
        ]);
        $song->artists()->sync($request->get('artist_id'), true);       
        return redirect()->route('be.song')->with(['status' => 'success','message' => 'Sửa Bài hát thành công']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $song = SongList::findorFail($id);
        $song->artists()->detach();
        $song->delete();
        return redirect()->route('be.song')->with(['status' => 'warning','message' => 'Xóa Bài Hát thành công']);
    }
}
