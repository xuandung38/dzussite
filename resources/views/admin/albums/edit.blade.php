@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Album</h1>
@stop

@section('content')
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Sửa Album</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('be.album.edit',$album->id) }}" method="POST">
                        @csrf
                        @method('POST')
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tên Album</label>
                                <input class="form-control {{ $errors->has('title') ? 'has-error' : '' }}" name="title" value="{{ old('title',$album->title) }}"> @if ($errors->has('title'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span> @endif
                            </div>

                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea id="my-editor" class="form-control {{ $errors->has('descriptions') ? 'has-error' : '' }}" name="descriptions">
                                {!! old('descriptions',$album->descriptions) !!}
                            </textarea>
                                @if ($errors->has('descriptions'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('descriptions') }}</strong>
                            </span> @endif
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Image</label>
                                <div class="input-group">
                                <span class="input-group-btn">
                             <a id="lmfimage" data-input="images" class="btn btn-primary ">
                               <i class="fa fa-picture-o"></i> Choose
                             </a>
                           </span>
                                    <input id="images" class="form-control" value="{{ old('images',$album->images) }}" type="text" name="images">
                                </div>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                     <strong>{{ $errors->first('title') }}</strong>
                                 </span> @endif
                            </div>
                            <div class="form-group">
                                <label>Multiple</label>
                                <select class="form-control select2" multiple="multiple" name="artist_id[]" data-placeholder="Chọn ca sỹ"
                                        style="width: 100%;">
                                    @foreach($artists as $artist)
                                        <option value="{{ $artist->id }}"
                                                @if(old('permission',in_array($artist->id,$album->artists()->pluck('id','id')->toArray()))) selected @endif>
                                            {{ $artist->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tags SEO:</label>
                                <input class="form-control {{ $errors->has('tags') ? 'has-error' : '' }}" name="tags" value="{{ old('tags',$album->tags) }}"> @if ($errors->has('tags'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('tags') }}</strong>
                        </span> @endif
                            </div>

                            <!-- /.form-group -->

                            <div class="form-group">
                                <button type="submit" name="submit" class="form-control btn btn-block btn-success">Lưu</button>
                                </span>

                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>

                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
                </div>
            </div>
            <!-- /.box -->

    </section>
@stop
@section('js')
    <script>
        $('.select2').select2()
        $('#lmfimage').filemanager('image');
        CKEDITOR.replace('my-editor', options);
    </script>
@stop
