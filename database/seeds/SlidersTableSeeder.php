<?php

use Illuminate\Database\Seeder;
use App\Models\Sliders;
use Faker\Factory;

class SlidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
    	$items = [
	        ['image' => $faker->imageUrl(), 'link' => $faker->url()],
	        ['image' => $faker->imageUrl(), 'link' => $faker->url()],
	    ];
	    foreach ($items as $item) {
	         Sliders::create($item);
	    }
    }
}
