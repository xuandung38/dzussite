<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(AlbumSeeder::class);
        // $this->call(AboutsTableSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(UserSeeder::class);
        // $this->call(SlidersTableSeeder::class);
        // $this->call(ArtistTableSeeder::class);
        // $this->call(ServicesTableSeeder::class);
        // $this->call(MusicAlbumTableSeeder::class);
        // $this->call(SongListTableSeeder::class);
        // $this->call(ShopCategoriesTableSeeder::class);
        // $this->call(ShopProductTableSeeder::class);
    }
}
