<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Models\ShopProduct;
use App\Models\ShopCategories;

class ShopProductsController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shopproducts = ShopProduct::with('category')->paginate(50);
        return view('admin.shopproducts.list',compact('shopproducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $cat = ShopCategories::all();
       return view('admin.shopproducts.create', compact('cat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shop =  new ShopProduct();
        $shop->create([
            'title' => $request->input('title'),
            'slug' => Str::slug($request->input('title'),'-'),
            'thumbnail' => $request->input('thumbnail'),
            'descriptions' => $request->input('descriptions'),
            'tags' => $request->input('tags'),
            'price' => $request->input('price'),
            'category_id' => $request->input('category_id'),
        ]);
        return redirect()->route('be.shop')->with(['status' => 'success','message' => 'Thêm Shop thành công']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = ShopCategories::all();
        $shopproduct = ShopProduct::findorFail($id);
        return view('admin.shopproducts.edit', compact('shopproduct', 'cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $shopproduct = ShopProduct::findorFail($id);
        $shopproduct->update([
            'title' => $request->input('title'),
            'slug' => Str::slug($request->input('title'),'-'),
            'thumbnail' => $request->input('thumbnail'),
            'descriptions' => $request->input('descriptions'),
            'tags' => $request->input('tags'),
            'price' => $request->input('price'),
            'category_id' => $request->input('category_id'),
        ]);
        return redirect()->route('be.shop')->with(['status' => 'success','message' => 'Sửa thông tin shop thành công']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShopProduct::findorFail($id)->delete();
        return redirect()->route('be.shop')->with(['status' => 'warning','message' => 'Xóa shop thành công']);
    }
}
