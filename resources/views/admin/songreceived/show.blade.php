@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>{{ $song->song_name }}</h1>
@stop

@section('content')
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Thông tin bài : {{ $song->song_name }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="card">
                    <div class="card-body">
                        <p>Kênh Phát hành: {{ $song->chanel->name }}</p>
                        <p>Link download : <a href="{{ $song->link }}" target="_blank">[Tải về]</a></p>
                        <p>Nghệ danh: {{ $song->customer->nick_name ?? '' }}</p>
                        <p>Họ tên: {{ $song->customer->full_name ?? '' }}</p>
                        <p>Email: <a href="mailto:{{ $song->email ?? '' }}">{{ $item->email ?? '' }}</a> </p>
                        <p>Số điện thoại: {{ $song->customer->phone ?? '' }}</p>
                        <p>Credit:   {{ $song->credit }}
                        </p>
                        <p>Lyric:</p>
                        <p>{{ $song->lyric }}</p>
                    </div>
                </div>
                <a href="{{ route('be.songs_received') }}" class="btn btn-danger">Quay về</a>
            </div>
        </div>
    </section>
@stop
