<div style="
    width: 100%;
    height: 500px;
    background: #CCCCCC;
    margin:0;
    padding: 100px 0 0 0;
    align-items: center;
    font-family: Arial, serif;
">
    <div style="
        width: 500px;
        background: rgba(255, 255, 255, 1);
        box-shadow: 0 5px 20px -5px rgba(0, 0, 0, 0.1);
        margin: 0 auto;
        padding: 10px 30px;
        border-radius: 10px;
    ">
        <h2 style="
            color: green;
            font-family: Arial, serif;
            text-align: center;
        ">Yêu cầu phát hành nhạc trên "{{ $song->chanel->name }}" từ {{ $song->customer->nick_name }}</h2>
        <p style="font-family: Arial, serif;">Tên bài hát: {{ $song->song_name }}</p>
        <p style="font-family: Arial, serif;">Link: <a href="{{ $song->link }}" target="_blank">{{ $song->link }}</a></p>
        <p style="font-family: Arial, serif;">Nghệ danh: {{ $song->customer->nick_name }}</p>
        <p style="font-family: Arial, serif;">Họ tên: {{ $song->customer->full_name }}</p>
        <p style="font-family: Arial, serif;">Email: <a href="mailto:{{ $song->email }}?subject=Feedback {{ $song->song_name }}">{{ $song->email }}</a></p>
        <p style="font-family: Arial, serif;">Điện thoại: {{ $song->customer->phone }}</p>
        <p style="font-family: Arial, serif;">Credit: {{ $song->credit }}</p>
    </div>
</div>
