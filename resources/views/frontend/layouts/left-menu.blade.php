<ul style="height: 150%" class="navbar-nav sidebar sidebar-dark accordion navbar-fixed-top" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('') }}">
                <div class="sidebar-brand-icon">
                    <img src="{{$settingsite->logo}}" class="img-fluid" />
                </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="{{route('listsong')}}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>{{trans('song.controller_singular_name')}}</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('artist')}}">
                    <i class="fas fa-fw fa-users"></i>
                    <span>{{trans('artist.controller_singular_name')}}</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('service')}}">
                    <i class="fas fa-fw fa-list"></i>
                    <span>{{trans('service.controller_singular_name')}}</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('about')}}">
                    <i class="fas fa-fw fa-info"></i>
                    <span>{{trans('about.controller_singular_name')}}</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('chanel')}}">
                    <i class="fas fa-fw fa-paper-plane"></i>
                    <span>{{trans('song_received.controller_singular_name')}}</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('shopproduct')}}">
                    <i class="fas fa-fw fa-shopping-bag"></i>
                    <span>{{trans('shop.controller_singular_name')}}</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('home.index')}}">
                    <i class="fas fa-fw fa-calendar-week"></i>
                    <span>{{trans('event.controller_singular_name')}}</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
            <li class="nav-item">
                <a class="nav-link" href="tel:0911830222">
                    <i class="fas fa-phone-volume"></i>
                    0911830222
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="tel:0911830222">
                <i class="fa fa-map-marker"></i>
                283 Khương Trung, Thanh Xuân, Hà Nội
                </a>
            </li>
        </ul>
