@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Sự Kiện</h1>
@stop

@section('content')
  
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-success" href="{{ route('be.slide.create') }}"> Thêm Event</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="listdata" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Avatar</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($slide as $key => $item)
                            <tr>
                                <td>{{$key++}}</td>
                                <td><img src="{{$item->image}}" class="img-circle img-thumbnail" width="30px"></td>
                                <td>
                                    <a class="btn btn-success" href="{{ route('be.slide.edit', $item->id) }}"> Sửa </a>
                                    <a class="btn btn-danger" href="{{ route('be.slide.delete', $item->id) }}"> Xóa </a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Avatar</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        $(function () {
            $('#listdata').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@stop

