@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Gửi mail tới khách hàng</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                     Thành công, mail đang được gửi tới khách hàng.
                </div>
            </div>

        </div>
    </div>
@stop
