@extends('frontend.layouts.app')
@section('content')
<!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <!--Chuyển tiếp mới phát hành-->
                <div class="my-5">
                    <div class="container">
                        <div class="auto-resizable-iframe mb-4">
                            <div>
                                <iframe height="538" src="https://www.youtube.com/embed/{{$detail->youtube_id}}" id="ytplayer" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="IntoNR__Info">
                            <h4 class="h4 mt-2">
                                <a href="" class="font-weight-bold text-dark">
                                    {{$detail->title}}</a>
                            </h4>
                            <em>
                                <a href="#" class="text-dark"></a>
                            </em>
                            <p>
                                {!! $detail->descriptions !!}
                            </p>
                            <p>
                                {!! $detail->lyrics !!}
                            </p>
                            <div>
                                <a class="mr-4 text-dark" href="{{$detail->download_link}}">
                                    <i class="fas fa-fw fa-stream mr-1"></i>{{trans('song.stream')}}
                                </a>
                                <a class="text-dark" href="{{$detail->download_link}}">
                                    <i class="fas fa-fw fa-download mr-1"></i>{{trans('song.download')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Main Content -->
@endsection
