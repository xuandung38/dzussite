<?php

namespace App\Mail;

use App\Models\SongReceived;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param SongReceived $song
     */
    public function __construct(SongReceived $song)
    {
        $this->song = $song->load('customer');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('feedback@dzusrecords.com', 'Dzus Feedback')->view('emails.feedback', ['song' => $this->song])
            ->subject('Cảm ơn bạn đã gửi bài hát '.$this->song->song_name.' đến DZUS Records!');
    }
}
