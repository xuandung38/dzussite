@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nghệ sỹ</h1>
@stop

@section('content')
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Sửa nghệ sỹ</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('be.artist.edit',$artist->id) }}" method="POST">
                        @csrf @method('POST')
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tên - Nghệ danh</label>
                                <input class="form-control {{ $errors->has('name') ? 'has-error' : '' }}" name="name" value="{{ old('name',$artist->name) }}"> @if ($errors->has('name'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span> @endif
                            </div>

                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea id="my-editor" class="form-control {{ $errors->has('descriptions') ? 'has-error' : '' }}" name="descriptions">
                                {!! old('descriptions',$artist->descriptions) !!}
                            </textarea>
                                @if ($errors->has('descriptions'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('descriptions') }}</strong>
                        </span> @endif
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Avatar</label>
                                <div class="input-group">
                                <span class="input-group-btn">
                             <a id="lmfimage" data-input="image" class="btn btn-primary ">
                               <i class="fa fa-picture-o"></i> Choose
                             </a>
                           </span>
                                    <input id="image" class="form-control" value="{{ old('image',$artist->image) }}" type="text" name="image">
                                </div>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                     <strong>{{ $errors->first('title') }}</strong>
                                 </span> @endif
                            </div>
                             <div class="form-group">
                                <label>Avatar Cover</label>
                                <div class="input-group">
                                <span class="input-group-btn">
                             <a id="lmfimage_cover" data-input="image_cover" class="btn btn-primary ">
                               <i class="fa fa-picture-o"></i> Choose
                             </a>
                           </span>
                                    <input id="image_cover" class="form-control" value="{{ old('image_cover', $artist->image_cover) }}" type="text" name="image_cover">
                                </div>
                                @if ($errors->has('image_cover'))
                                    <span class="help-block">
                                     <strong>{{ $errors->first('image_cover') }}</strong>
                                 </span> @endif
                            </div>
                            <div class="form-group">
                                <label>Tags SEO:</label>
                                <input class="form-control {{ $errors->has('tags') ? 'has-error' : '' }}" name="tags" value="{{ old('tags',$artist->tags) }}"> @if ($errors->has('tags'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('tags') }}</strong>
                        </span> @endif
                            </div>
                            <div class="form-group">
                                <label>Link FB: </label>
                                <input class="form-control {{ $errors->has('facebook') ? 'has-error' : '' }}" name="facebook" value="{{ old('facebook',$artist->facebook) }}"> @if ($errors->has('facebook'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('facebook') }}</strong>
                        </span> @endif
                            </div>
                            <div class="form-group">
                                <label>Link Instagram: </label>
                                <input class="form-control {{ $errors->has('instargram') ? 'has-error' : '' }}" name="instargram" value="{{ old('instargram',$artist->instargram) }}"> @if ($errors->has('instargram'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('instargram') }}</strong>
                        </span> @endif
                            </div>
                            <div class="form-group">
                                <label>Link Youtube: </label>
                                <input class="form-control {{ $errors->has('youtube') ? 'has-error' : '' }}" name="youtube" value="{{ old('youtube',$artist->youtube) }}"> @if ($errors->has('youtube'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('youtube') }}</strong>
                        </span> @endif
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <button type="submit" name="submit" class="form-control btn btn-block btn-success">Lưu</button>
                                </span>

                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>

                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
                </div>
            </div>
            <!-- /.box -->

    </section>
@stop
@section('js')
    <script>
        $('#lmfimage_cover').filemanager('image_cover');
        $('#lmfimage').filemanager('image');
        CKEDITOR.replace('my-editor', options);
    </script>
@stop
