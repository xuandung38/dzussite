<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::group(['namespace'=>'Backend', 'prefix' => 'admin' ,'middleware' => 'auth', 'as' => 'be.'], function () {

    //    Setting main
    Route::get('/','SettingSiteController@index')->name('settings');
    Route::post('/','SettingSiteController@update');

    // Route About
    Route::get('about','AboutController@index')->name('about');
    Route::post('about','AboutController@update');

    //    slider
    Route::group(['prefix' => 'slider'],function ()
    {
            Route::get('','SlidersController@index')->name('slide');
            Route::get('create','SlidersController@create')->name('slide.create');
            Route::post('create','SlidersController@store');
            Route::get('edit/{id}','SlidersController@edit')->name('slide.edit');
            Route::post('edit/{id}','SlidersController@update');
            Route::get('delte/{id}','SlidersController@destroy')->name('slide.delete');
    });
    //    artist
    Route::group(['prefix' => 'artist'],function (){
        Route::get('','ArtistsController@index')->name('artist');
        Route::get('create','ArtistsController@create')->name('artist.create');
        Route::post('create','ArtistsController@store');
        Route::get('edit/{id}','ArtistsController@edit')->name('artist.edit');
        Route::post('edit/{id}','ArtistsController@update');
        Route::get('delete/{id}','ArtistsController@destroy')->name('artist.delete');
    });
    //    albums
    Route::group(['prefix' => 'album'],function (){
        Route::get('','MusicAlbumController@index')->name('album');
        Route::get('create','MusicAlbumController@create')->name('album.create');
        Route::post('create','MusicAlbumController@store');
        Route::get('edit/{id}','MusicAlbumController@edit')->name('album.edit');
        Route::post('edit/{id}','MusicAlbumController@update');
        Route::get('delete/{id}','MusicAlbumController@destroy')->name('album.delete');
    });
    //Shop Categories
    Route::group(['prefix' => 'shopcategorie'],function (){
        Route::get('','ShopCategoriesController@index')->name('shopcategorie');
        Route::get('create','ShopCategoriesController@create')->name('shopcategorie.create');
        Route::post('create','ShopCategoriesController@store');
        Route::get('edit/{id}','ShopCategoriesController@edit')->name('shopcategorie.edit');
        Route::post('edit/{id}','ShopCategoriesController@update');
        Route::get('delete/{id}','ShopCategoriesController@destroy')->name('shopcategorie.delete');
    });
    //Service
    Route::group(['prefix' => 'service'],function (){
        Route::get('','ServicesController@index')->name('service');
        Route::get('create','ServicesController@create')->name('service.create');
        Route::post('create','ServicesController@store');
        Route::get('edit/{id}','ServicesController@edit')->name('service.edit');
        Route::post('edit/{id}','ServicesController@update');
        Route::get('delete/{id}','ServicesController@destroy')->name('service.delete');
    });

    //Shop
    Route::group(['prefix' => 'shop'],function (){
        Route::get('','ShopProductsController@index')->name('shop');
        Route::get('create','ShopProductsController@create')->name('shop.create');
        Route::post('create','ShopProductsController@store');
        Route::get('edit/{id}','ShopProductsController@edit')->name('shop.edit');
        Route::post('edit/{id}','ShopProductsController@update');
        Route::get('delete/{id}','ShopProductsController@destroy')->name('shop.delete');
    });

    //Song
    Route::group(['prefix' => 'song'],function (){
        Route::get('','SongListController@index')->name('song');
        Route::get('create','SongListController@create')->name('song.create');
        Route::post('create','SongListController@store');
        Route::get('edit/{id}','SongListController@edit')->name('song.edit');
        Route::post('edit/{id}','SongListController@update');
        Route::get('delete/{id}','SongListController@destroy')->name('song.delete');
    });

    Route::resource('chanel', 'MusicChanelController');
    //Shortlink
    Route::group(['prefix' => 'shortlink'],function (){
        Route::get('','ShortLinkController@index')->name('shortlink');
        Route::get('create','ShortLinkController@create')->name('shortlink.create');
        Route::post('create','ShortLinkController@store');
        Route::get('edit/{id}','ShortLinkController@edit')->name('shortlink.edit');
        Route::post('edit/{id}','ShortLinkController@update');
        Route::get('delete/{id}','ShortLinkController@destroy')->name('shortlink.delete');
    });

    //Shortlink
    Route::group(['prefix' => 'songs_received'],function (){
        Route::get('','SongReceivedController@index')->name('songs_received');
        Route::get('show/{id}','SongReceivedController@show')->name('song_show');

    });
    //Customer
    Route::group(['prefix' => 'customer'],function (){
        Route::get('send_mail','CustomerController@requestSendMail')->name('send_mail_cus');
        Route::post('send_mail','CustomerController@sendMail');
    });

});
Route::group(['namespace'=>'Frontend', 'middleware' => 'web'], function () {
    Route::get('',  'IndexController@services');
    Route::get('service', 'IndexController@services')->name('service');
    Route::get('event', 'IndexController@index')->name('home.index');
    Route::get('about', 'IndexController@about')->name('about');
    Route::get('shop', 'IndexController@shopProduct')->name('shopproduct');
    Route::get('shop/{slug}.html', 'IndexController@detailSong')->name('detailshop');
    Route::get('song', 'ListsongController@index')->name('listsong');
    Route::get('song/{slug}.html', 'ListsongController@detailSong')->name('detailsong');
    Route::get('artist', 'ArtistController@index')->name('artist');
    Route::get('artist/{slug}.html', 'ArtistController@detailArtist')->name('detailartist');
    Route::get('submission', 'MusicChanelController@index')->name('chanel');
    Route::get('submission/{slug}.html', 'SongReceivedController@index')->name('sendsong');
    Route::post('store-song', 'SongReceivedController@storeSong')->name('storesong');
    Route::get('customer', 'SongReceivedController@getCustomer')->name('getcustomer');
    Route::get('{code}', 'IndexController@shortLink')->name('home.shortlink');
});


