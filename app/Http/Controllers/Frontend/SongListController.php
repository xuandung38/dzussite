<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SongList;

class ListsongController extends Controller
{
    public function index()
    {
        $songlist = SongList::orderBy('created_at', 'desc')->paginate(6);
    	return view('frontend.song.index', ['songlist' => $songlist]);
    }

    public function detailSong($slug)
    {
    	$detail = SongList::with('artists')->where('slug', '=', $slug)->first();
        return view('frontend.song.detail', ['detail' => $detail]);
    }
}
