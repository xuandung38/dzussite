@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Shop</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-success" href="{{ route('be.shop.create') }}"> Thêm Shop</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="listdata" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Avatar</th>
                            <th>Title</th> 
                            <th>Danh Mục</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($shopproducts as $key => $item)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td><img src="{{ $item->thumbnail }}" class="img-circle img-thumbnail" width="30px"></td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->category->title }}</td>
                                <td>
                                    <a class="btn btn-success" href="{{ route('be.shop.edit',$item->id) }}"> Sửa </a>
                                    <a class="btn btn-danger" href="{{ route('be.shop.delete',$item->id) }}"> Xóa </a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Avatar</th>
                            <th>Title(s)</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                    {{ $shopproducts->links() }}
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        $(function () {
            $('#listdata').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
>>>>>>> 1e243efaea420dd462aade6786364be1223a21a6
@stop

