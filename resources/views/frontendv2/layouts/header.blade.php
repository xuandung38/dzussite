<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="">

    <title>{{$settingsite->title}}</title>
    <meta name="description" content="{{$settingsite->descriptions}}">
    <meta name="keywords" content="{{$settingsite->tags}}">
    <meta property="og:image" content="{{$settingsite->ogimage}}" />
    <meta property="og:image:url" content="{{$settingsite->ogimage}}" />
    <meta property="og:image:secure_url" content="{{$settingsite->ogimage}}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{$settingsite->title}}" />
    <meta property="og:description" content="{{$settingsite->descriptions}}" />
    <meta property="og:url" content="{{ url('') }}" />
    <meta property="og:site_name" content="{{$settingsite->title}}" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="{{$settingsite->title}}" />
    <meta name="twitter:image" content="{{$settingsite->ogimage}}"/>
    <meta name="twitter:description" content="{{$settingsite->descriptions}}" />

    <!-- Custom fonts for this template-->
    <link href="{{asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('vendor/owl/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/owl/assets/owl.theme.default.min.css')}}">

    <link rel="stylesheet" href="{{asset('resources/css/style.css')}}">
</head>
