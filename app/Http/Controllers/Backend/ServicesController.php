<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Services;
use Illuminate\Support\Str;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Services::paginate(50);
        return view('admin.services.list',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service =  new Services();
        $service->create([
            'title' => $request->input('title'),
            'slug' => Str::slug($request->input('title'),'-'),
            'images' => $request->input('images'),
            'descriptions' => $request->input('descriptions'),
            'tags' => $request->input('tags'),
            'soundclouds' => $request->input('soundclouds'),
        ]);
        return redirect()->route('be.service')->with(['status' => 'success','message' => 'Thêm dịch vụ thành công']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Services::findorFail($id);
        return view('admin.services.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Services::findorFail($id);
        $service->update([
            'title' => $request->input('title'),
            'slug' => Str::slug($request->input('title'),'-'),
            'images' => $request->input('images'),
            'descriptions' => $request->input('descriptions'),
            'tags' => $request->input('tags'),
            'soundclouds' => $request->input('soundclouds'),
        ]);
        return redirect()->route('be.service')->with(['status' => 'success','message' => 'Thêm dịch vụ thành công']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Services::findorFail($id)->delete();
        return redirect()->route('be.service')->with(['status' => 'warning','message' => 'Xóa dịch vụ thành công']);
    }
}
