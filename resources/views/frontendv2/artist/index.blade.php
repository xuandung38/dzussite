@extends('frontend.layouts.app')
@section('content')
 <!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <!--Mới phát hành-->
                <div class="my-5">
                    <div class="container">
                        <div class="text-center">
                            <h2 class="text-uppercase title-section">Nghệ Sĩ</h2>
                            <p class="mb-5 text-muted">
                                Các nghệ sĩ đã phát hành trên DZUS Records
                            </p>
                        </div>
                        <div class="row mb-5">
                        	@foreach($artists as $artist)
                            <div class="col-md-4">
                                <div class="mb-3">
                                    <a href="{{route('detailartist', ['slug' => $artist->slug])}}">
                                        <img src="{{$artist->image}}" class="img-fluid">
                                    </a>
                                </div>
                                <div class="text-center">
                                    <h5 class="text-uppercase">{{$artist->name}}</h5>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <nav class="my-4">
                            {!!$artists->links()!!}
                        </nav>
                    </div>
                </div>

            </div>
@endsection