<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopProduct extends Model
{
    protected $fillable = [
        'title','slug','thumbnail','price','descriptions','tags','category_id'
    ];

    public function category()
    {
        return $this->hasOne(ShopCategories::class,'id','category_id');
    }
} 
