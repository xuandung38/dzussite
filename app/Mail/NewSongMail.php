<?php

namespace App\Mail;

use App\Models\SongReceived;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewSongMail extends Mailable
{
    use Queueable, SerializesModels;

    public $song;

    public function __construct(SongReceived $song)
    {
        $this->song = $song->load(['customer','chanel']);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.new_song', ['song' => $this->song])
            ->subject($this->song->song_name);
    }
}
