<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'full_name',
        'nick_name',
        'email',
        'phone',
    ];

    public function resolveRouteBinding($value)
    {
        // Retrieve by id or key.
        return is_numeric($value)
            ? $this->where('id', $value)->first()
            : $this->where('email', $value)->first();
    }

}
