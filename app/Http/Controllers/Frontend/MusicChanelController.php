<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\MusicChanel;
use Illuminate\Http\Request;

class MusicChanelController extends Controller
{
    public function index()
    {
        $chanels = MusicChanel::where('status', 1)->get();
        return view('frontend.chanel.index', compact('chanels'));
    }

}
