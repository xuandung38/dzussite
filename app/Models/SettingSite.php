<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingSite extends Model
{
    protected $fillable = [
        'title','logo','descriptions','tags','address','hotline','maps', 'facebook', 'instargram', 'youtube', 'spotify', 'ogimage'
    ];
}
