<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopCategories extends Model
{
    protected $fillable = [
        'title','slug','descriptions'
    ];

    // public function products()
    // {
    //     return $this->hasMany(ShopProduct::class,'category_id','id')
    // }
}
