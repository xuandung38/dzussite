<?php

use Illuminate\Database\Seeder;
use App\Models\SettingSite;

class SettingSiteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SettingSite::create([
        	'title' => '1',
        	'logo'  => 'http://dev.dzussite.leon.vm/resources/img/Navbar/1.png',
        	'descriptions' => '1',
        	'tags' => '1',
        	'address' => '1',
        	'hotline' => '1',
        	'social_link' => '1',
        	'maps' => '1'
        ]);
    }
}
