<?php

namespace App\Http\Controllers\Backend;

use App\Models\Artists;
use App\Models\MusicAlbum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class MusicAlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = MusicAlbum::paginate(50);
        return view('admin.albums.list',compact('albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $artists = Artists::all();
        return view('admin.albums.create',compact('artists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $album = new MusicAlbum();
        $album->title = $request->get('title');
        $album->slug = Str::slug($request->get('title'),'-');
        $album->images = $request->get('images');
        $album->descriptions = $request->get('descriptions');
        $album->tags = $request->get('tags');
        $album->save();
        $album->artists()->sync($request->get('artist_id'), false);
        return redirect()->route('be.album')->with(['status' => 'success','message' => 'Thêm album thành công']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artists = Artists::all();
        $album = MusicAlbum::findorFail($id);
        return view('admin.albums.edit',compact('artists','album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $album = MusicAlbum::findorFail($id);
        $album->update([
            'title' => $request->input('title'),
            'slug' => Str::slug($request->input('title'),'-'),
            'images' => $request->input('images'),
            'descriptions' => $request->input('descriptions'),
            'tags' => $request->input('tags'),
        ]);
        $album->artists()->sync($request->get('artist_id'));
        return redirect()->route('be.album')->with(['status' => 'success','message' => 'Sửa album thành công']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $album = MusicAlbum::findorFail($id);
        $album->artists()->detach();
        $album->delete();
        return redirect()->route('be.album')->with(['status' => 'danger','message' => 'Xóa album thành công']);
    }
}
