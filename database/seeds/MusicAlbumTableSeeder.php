<?php

use Illuminate\Database\Seeder;
use App\Models\MusicAlbum;

class MusicAlbumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
        	[
        		'title' => 'Album A',
        		'slug'  => 'album a',
        		'images' => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'descriptions' => 'Mo ta',
        		'tags' => 'nhat, minh'
        	],
        	[
        		'title' => 'Album B',
        		'slug'  => 'album b',
        		'images' => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'descriptions' => 'Mo ta',
        		'tags' => 'nhat, minh'
        	],
        	[
        		'title' => 'Album C',
        		'slug'  => 'album c',
        		'images' => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'descriptions' => 'Mo ta',
        		'tags' => 'nhat, minh'
        	],

        ];

        foreach ($items as $item) {
        	MusicAlbum::create($item);
        }
    }
}
