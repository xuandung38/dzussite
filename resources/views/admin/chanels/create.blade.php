@extends('adminlte::page')

@section('title', 'Dashboard')


@section('content_header')
    <h1>Kênh phát hành</h1>
@stop

@section('content')
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Thêm Kênh</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('be.chanel.store') }}" method="POST">
                        @csrf
                        @method('POST')
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tên Kênh</label>
                                <input class="form-control {{ $errors->has('name') ? 'has-error' : '' }}" name="name"
                                       value="{{ old('name') }}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Email Kênh</label>
                                <input class="form-control {{ $errors->has('email') ? 'has-error' : '' }}" name="email"
                                       value="{{ old('email') }}" type="email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Chanel Link</label>
                                <input class="form-control {{ $errors->has('chanel_url') ? 'has-error' : '' }}" name="chanel_url"
                                       value="{{ old('chanel_url') }}">
                                @if ($errors->has('chanel_url'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('chanel_url') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea id="my-editor" class="form-control {{ $errors->has('descriptions') ? 'has-error' : '' }}" name="descriptions">
                                {!! old('descriptions') !!}
                                </textarea>
                                @if ($errors->has('descriptions'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('descriptions') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Avatar Kênh</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                         <a  data-input="avatar" class="btn btn-primary lmfimage">
                                           <i class="fa fa-picture-o"></i> Choose
                                         </a>
                                    </span>
                                    <input id="avatar" class="form-control" value="{{ old('avatar') }}" type="text"  name="avatar">
                                </div>
                                @if ($errors->has('avatar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('avatar') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Ảnh bìa chanel</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                         <a  data-input="cover" class="btn btn-primary lmfimage">
                                           <i class="fa fa-picture-o"></i> Choose
                                         </a>
                                    </span>
                                    <input id="cover" class="form-control" value="{{ old('cover') }}" type="text"  name="cover">
                                </div>
                                @if ($errors->has('cover'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cover') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Status:</label>
                                <select class="form-control {{ $errors->has('status') ? 'has-error' : '' }}" name="status">
                                    <option value="1">On</option>
                                    <option value="0">Off</option>
                                </select>
                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <!-- /.form-group -->

                            <div class="form-group">
                                <button type="submit" name="submit" class="form-control btn btn-block btn-success">Lưu</button>

                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>

                    </form>
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.box -->

    </section>
@stop
@section('js')
    <script>
        $('.lmfimage').filemanager('image');
        CKEDITOR.replace('my-editor', options);
    </script>

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\Backend\StoreMusicChanelRequest') !!}
@stop
