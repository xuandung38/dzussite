@extends('frontend.layouts.app')
@section('content')
<!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <!--Mới phát hành-->
                <div class="my-5">
                    <div class="container">
                        <div class="text-center">
                            <h2 class="text-uppercase title-section">Dịch Vụ</h2>
                            <p class="mb-5 text-muted">
                                Các dịch vụ đã phát hành trên DZUS Records
                            </p>
                        </div>

                        <div class="row">
                            @foreach($services as $service)
                            <div class="col-lg-4 mb-4">
                                <div class="card shadow">
                                    <img src="{{$service->images}}" class="card-img-top">
                                    <div class="card-body">
                                        <div>
                                            <h5 class="h5 font-weight-bold">{{$service->title}}</h5>
                                            <p>{!!$service->descriptions!!}</p>
                                            <a href="{{ $service->soundclouds }}"><span><i class="far fa-play-circle"></i>Thử Nhạc</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <nav class="my-4">
                            {!!$services->links()!!}
                        </nav>

                    </div>
                </div>

            </div>
            <!-- End of Main Content -->
@endsection