<?php

use Illuminate\Database\Seeder;
use App\Models\ShopCategories;
class ShopCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
        	[
        		'title' => 'Âm nhạc A',
        		'slug' => 'am-nhac-a',
        		'descriptions' => 'Âm nhạc rất là hay'
        	],
        	[
        		'title' => 'Âm nhạc B',
        		'slug' => 'am-nhac-b',
        		'descriptions' => 'Âm nhạc rất là hay'
        	],
        	[
        		'title' => 'Âm nhạc C',
        		'slug' => 'am-nhac-c',
        		'descriptions' => 'Âm nhạc rất là hay'
        	],
        ];
        foreach ($items as $item) {
        	ShopCategories::create($item);
        }
        
    }
}
