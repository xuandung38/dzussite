<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MusicAlbum extends Model
{
    protected $table = 'music_albums';

    protected $guarded = [];

    public function artists()
    {
        return $this->belongsToMany(Artists::class,'artist_album');
    }

    public function songs()
    {
        return $this->hasMany(SongList::class,'album_id','id');
    }


}
