@extends('frontend.layouts.app')
@section('content')
            <div id="content">

                <!-- Begin Page Content -->
                <!--Mới phát hành-->
                <div class="my-5">
                    <div class="container">
                        <div class="mb-4">
                            <div style="background: url({{$detail->image_cover}}) center center / cover no-repeat; height: 220px;"></div>
                        </div>
                        <div class="mb-4">
                            <div class="h4 font-weight-bold mt-2">
                                Giới thiệu
                            </div>
                            <p class="font-weight-bold">{{$detail->name}}</p>
                            <p>
                                {!!$detail->descriptions!!}
                            </p>
                             <span >
				            	<a class="white-text" href="{{$detail->facebook}}"><i class="fab fa-facebook-square"></i> Facebook </a>
				            	<a class="white-text" href="{{$detail->instargram}}"><i class="fab fa-instagram"></i> Instargram </a>
				            	<a class="white-text" href="{{$detail->youtube}}"><i class="fab fa-youtube-square"></i> Youtube </a>
				            </span>
                        </div>

                        <div class="h4 mb-4 font-weight-bold">
                            Bài hát của {{$detail->name}}</span>
                        </div>
                         @foreach($list_song as $song) 
                            <div class="media mb-4">
                                    <a href="#">
                                         <img class="img-fluid w-200px mr-3 d-none d-sm-block" src="{{$song->images}}">
                                    </a>
                                <div class="media-body">
                                    <h4 class="h4 mt-2">
                                        <a href="{{route('detailsong', ['slug' => $song->slug])}}" class="font-weight-bold text-dark">{{$song->title}}</a>
                                    </h4>
                                    <em>
                                        <a href="{{route('detailsong', ['slug' => $song->slug])}}" class="text-dark">{{$song->title}}</a>
                                    </em>
                                    <p class="text2-overflow-dot">
                                        {!!$song->descriptions!!}
                                    </p>
                                    <div>
                                        <a class="mr-4 text-dark" href="{{$song->download_link}}">
                                            <i class="fas fa-fw fa-stream mr-1"></i>{{trans('song.stream')}}
                                        </a>
                                        <a class="text-dark" href="{{$song->download_link}}">
                                            <i class="fas fa-fw fa-download mr-1"></i>{{trans('song.download')}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <nav class="my-4">
                            {!!$list_song->links()!!}
                        </nav>
                    </div>
                </div>

            </div>
@endsection