@extends('frontend.layouts.app')
@section('content')
    <div id="content">

        <!-- Begin Page Content -->
        <!--Mới phát hành-->
        <div class="my-5">
            <div class="container">
                <div class="text-center">
                    <h2 class="text-uppercase title-section">Danh Sách Kênh Phát hành</h2>
                    <p class="mb-5 text-muted">
                        Các Kênh phát hành nhạc của chúng tôi
                    </p>
                </div>
                <div class="card-deck">
                    @foreach($chanels as $chanel)
                        <div class="col-md-4" style="margin-top: 15px">
                            <div class="card">
                                <div class="card-header"
                                     style="
                                         text-align: center;
                                         background-image: url({{ $chanel->cover }});
                                         background-position: center;
                                         background-repeat: no-repeat;
                                         background-size: cover;
                                         ">
                                    <img class="rounded-circle" src="{{ $chanel->avatar }}" alt="{{ $chanel->name }}" width="140" height="140" style="">

                                </div>
                                <div class="card-body">
                                    <h5 class="card-title text-center"><a href="{{ $chanel->chanel_url }}" target="_blank"> {{ $chanel->name}}</a></h5>
                                    <p class="card-text">{!! $chanel->descriptions  !!}</p>
                                </div>
                                <div class="card-footer">
                                    <a href="{{ route('sendsong',$chanel->slug) }}" target="_blank" class ="btn btn-danger btn-block">Gửi yêu cầu phát hành trên kênh này</a>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
