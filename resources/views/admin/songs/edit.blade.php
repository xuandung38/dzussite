@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Bài hát</h1>
@stop

@section('content')
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Sửa bài hát</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('be.song.edit', $song->id) }}" method="POST">
                        @csrf @method('POST')
                       <div class="col-md-6">
                            <div class="form-group">
                                <label>Tên - Bài Hát</label>
                                <input class="form-control {{ $errors->has('title') ? 'has-error' : '' }}" name="title" value="{{ old('title', $song->title) }}"> @if ($errors->has('name'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span> @endif
                            </div>
                             <div class="form-group">
                                <label>Album</label>
                                <select class="form-control select2" name="album_id" data-placeholder="Chọn ca sỹ"
                                        style="width: 100%;">
                                   @foreach($albums as $album)
                                       <option value="{{ $album->id }}"> {{ $album->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Artist</label>
                                <select class="form-control select2" multiple="multiple" name="artist_id[]" data-placeholder="Chọn ca sỹ"
                                        style="width: 100%;">
                                   @foreach($artists as $artist)
                                       <option value="{{ $artist->id }}"
                                                @if(old('artist_id',in_array($artist->id,$song->artists()->pluck('id','id')->toArray()))) selected @endif>
                                            {{ $artist->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                             <div class="form-group">
                                <label>Avatar</label>
                                <div class="input-group">
                                <span class="input-group-btn">
                             <a id="lmfimage" data-input="image" class="btn btn-primary ">
                               <i class="fa fa-picture-o"></i> Choose
                             </a>
                           </span>
                                    <input id="image" class="form-control" value="{{ old('images', $song->images) }}" type="text" name="images">
                                </div>
                                @if ($errors->has('images'))
                                    <span class="help-block">
                                     <strong>{{ $errors->first('images') }}</strong>
                                 </span> @endif
                            </div>

                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea id="editor" class="form-control {{ $errors->has('descriptions') ? 'has-error' : '' }}" name="descriptions">
                                {!! old('descriptions', $song->descriptions) !!}
                            </textarea>
                                @if ($errors->has('descriptions'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('descriptions') }}</strong>
                                    </span> 
                                @endif
                            </div>
                           
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                                <label>Nội dung</label>
                                <textarea id="my-editor" class="form-control {{ $errors->has('lyrics') ? 'has-error' : '' }}" name="lyrics">
                                {!! old('lyrics', $song->lyrics) !!}
                            </textarea>
                                @if ($errors->has('lyrics'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lyrics') }}</strong>
                                    </span> 
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Tags SEO:</label>
                                <input class="form-control {{ $errors->has('tags') ? 'has-error' : '' }}" name="tags" value="{{ old('tags', $song->tags) }}"> @if ($errors->has('tags'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('tags') }}</strong>
                        </span> @endif
                            </div>
                           
                            <div class="form-group">
                                <label>Link Download: </label>
                                <input class="form-control {{ $errors->has('download_link') ? 'has-error' : '' }}" name="download_link" value="{{ old('download_link', $song->download_link) }}"> @if ($errors->has('download_link'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('download_link') }}</strong>
                        </span> @endif
                            </div>
                            <div class="form-group">
                                <label>ID Youtube: </label>
                                <input class="form-control {{ $errors->has('youtube_id') ? 'has-error' : '' }}" name="youtube_id" value="{{ old('youtube_id', $song->youtube_id) }}"> @if ($errors->has('youtube_id'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('youtube_id') }}</strong>
                        </span> @endif

                         

                             
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <button type="submit" name="submit" class="form-control btn btn-block btn-success">Lưu</button>
                                </span>

                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>
                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
                </div>
            </div>
            <!-- /.box -->

    </section>
@stop
@section('js')
    <script>
        $('.select2').select2()
        $('#lmfimage').filemanager('image');
        CKEDITOR.replace('my-editor', options);
        CKEDITOR.replace('editor', options);
    </script>
@stop
