<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\SongReceived;

class SongReceivedController extends Controller
{
    public function index()
    {
        $songs = SongReceived::with('customer','chanel')->orderByDesc('id')->paginate('20');
        return view('admin.songreceived.list', compact('songs'));
    }

    public function show($id)
    {
        $song = SongReceived::with(['customer', 'chanel'])->find($id);
        return view('admin.songreceived.show', compact('song'));
    }
}
