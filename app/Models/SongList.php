<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Artists;
use App\Models\MusicAlbum;

class SongList extends Model
{
    protected $table = 'song_lists';
    protected $guarded = [];

    public function artists()
    {
        return $this->belongsToMany(Artists::class,'artist_songs');
    }
    public function album()
    {
        return $this->hasOne(MusicAlbum::class,'id','album_id');
    }
}
