<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistSongs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist_songs', function (Blueprint $table) {
            $table->unsignedBigInteger('artists_id');
            $table->foreign('artists_id')->references('id')->on('artists')->onDelete('CASCADE');
            $table->unsignedBigInteger('song_list_id');
            $table->foreign('song_list_id')->references('id')->on('song_lists')->onDelete('CASCADE');
            $table->primary(['artists_id', 'song_list_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artist_songs');
    }
}
