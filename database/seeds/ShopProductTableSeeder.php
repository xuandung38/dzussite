<?php

use Illuminate\Database\Seeder;
use App\Models\ShopProduct;
class ShopProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
        	[
        		'title'        => 'Âm nhạc A',
        		'slug'         => 'am-nhac-a',
        		'thumbnail'	   => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'images'       => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'price' 	   => '124',
        		'descriptions' => 'mo ta',
        		'tags' 		   => 'nhat',
       			'category_id'  => '1'
        	],
        	[
        		'title'        => 'Âm nhạc B',
        		'slug'         => 'am-nhac-b',
        		'thumbnail'	   => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'images'       => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'price' 	   => '124',
        		'descriptions' => 'mo ta',
        		'tags' 		   => 'nhat',
       			'category_id'  => '1'
        	],
        	[
        		'title'        => 'Âm nhạc C',
        		'slug'         => 'am-nhac-c',
        		'thumbnail'	   => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'images'       => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'price' 	   => '124',
        		'descriptions' => 'mo ta',
        		'tags' 		   => 'nhat',
       			'category_id'  => '1'
        	],
        	[
        		'title'        => 'Âm nhạc D',
        		'slug'         => 'am-nhac-d',
        		'thumbnail'	   => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'images'       => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'price' 	   => '124',
        		'descriptions' => 'mo ta',
        		'tags' 		   => 'nhat',
       			'category_id'  => '1'
        	],
        	[
        		'title'        => 'Âm nhạc V',
        		'slug'         => 'am-nhac-v',
        		'thumbnail'	   => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'images'       => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'price' 	   => '124',
        		'descriptions' => 'mo ta',
        		'tags' 		   => 'nhat',
       			'category_id'  => '1'
        	],
        	[
        		'title'        => 'Âm nhạc F',
        		'slug'         => 'am-nhac-f',
        		'thumbnail'	   => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'images'       => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'price' 	   => '124',
        		'descriptions' => 'mo ta',
        		'tags' 		   => 'nhat',
       			'category_id'  => '1'
        	],
        	[
        		'title'        => 'Âm nhạc J',
        		'slug'         => 'am-nhac-j',
        		'thumbnail'	   => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'images'       => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'price' 	   => '124',
        		'descriptions' => 'mo ta',
        		'tags' 		   => 'nhat',
       			'category_id'  => '1'
        	],
        	[
        		'title'        => 'Âm nhạc O',
        		'slug'         => 'am-nhac-o',
        		'thumbnail'	   => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'images'       => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg',
        		'price' 	   => '124',
        		'descriptions' => 'mo ta',
        		'tags' 		   => 'nhat',
       			'category_id'  => '1'
        	]
        ];
        foreach ($items as  $item) {
        	ShopProduct::create($item);
        }
    }
}
