@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Shop Categories</h1>
@stop

@section('content')
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Sửa Shop Categories</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('be.shopcategorie.edit',$shopcategorie->id) }}" method="POST">
                        @csrf @method('POST')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Shop Categories</label>
                                <input class="form-control {{ $errors->has('title') ? 'has-error' : '' }}" name="title" value="{!! old('title',$shopcategorie->title) !!}"> @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span> @endif
                             </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <button type="submit" name="submit" class="form-control btn btn-block btn-success">Lưu</button>
                                </span>

                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>

                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
                </div>
            </div>
            <!-- /.box -->

    </section>
@stop
@section('js')
    <script>
        $('#lmfimage').filemanager('image');
        CKEDITOR.replace('my-editor', options);
    </script>
@stop
