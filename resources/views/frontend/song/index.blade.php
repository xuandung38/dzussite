@extends('frontend.layouts.app')
@section('content')
<!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <!--Mới phát hành-->
                <div class="my-5">
                    <div class="container">
                        <div class="text-center">
                            <h2 class="text-uppercase title-section">Bài Hát</h2>
                            <p class="mb-5 text-muted">
                                Các bài hát đã phát hành trên DZUS Records
                            </p>
                        </div>
						@foreach($songlist as $song)
						
						<div class="media mb-4">
                            <a href="{{route('detailsong', ['slug' => $song->slug])}}">
                                <img class="img-fluid w-200px mr-3 d-none d-sm-block" src="{{ $song->images  }}" style="width: 60px%;">
                            </a>
                            <div class="media-body">
                                <h4 class="h4 mt-2">
                                    <a href="{{route('detailsong', ['slug' => $song->slug])}}" class="font-weight-bold text-dark">{{$song->title}}</a>
                                </h4>
                                <em>
                                    @foreach($song->artists as $artist)
                                        <a href="{{route('detailartist', ['slug' => $artist->slug])}}" class="text-dark">
                                        {{$artist->name}} ,
                                        </a>
                                    @endforeach
                                </em>
                                <p class="text2-overflow-dot">
                                    {!! $song->descriptions !!}
                                </p>
                                <div>
                                    <a class="mr-4 text-dark" href="{{ $song->download_link }}" target="_blank">
                                        <i class="fas fa-fw fa-stream mr-1"></i>{{trans('song.stream')}}
                                    </a>
                                     <a class="mr-4 text-dark" href="{{ $song->download_link }}" target="_blank">
                                        <i class="fas fa-fw fa-download mr-1"></i>{{trans('song.download')}}
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        
						@endforeach
                        <nav class="my-4">
                            {!!$songlist->links()!!}
                        </nav>
                    </div>
                </div>

            </div>
            <!-- End of Main Content -->
@endsection