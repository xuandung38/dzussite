@extends('frontend.layouts.app')
@section('content')
<div id="content">

                <!-- Begin Page Content -->
                <!--Mới phát hành-->
                <div class="my-5">
                    <div class="container">
                        <div class="text-center">
                            <h2 class="text-uppercase title-section">Giới Thiệu</h2>
                            <p class="mb-5 text-muted">
                                @if(!empty($about))
                                    {{$about->title}}
                                @endif
                            </p>
                        </div>
                        @if(!empty($about))
                            {!! $about->contents !!}
                        @endif
                    </div>
                </div>

            </div>
@endsection
