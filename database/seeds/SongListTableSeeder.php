<?php

use Illuminate\Database\Seeder;
use App\Models\SongList;
use Faker\Factory;
use Illuminate\Support\Str;

class SongListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $faker->addProvider(new Faker\Provider\Youtube($faker));

        $items = [
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],
        	[
        		'title' =>  $faker->name,
        		'slug'  =>  Str::slug($faker->name),
        		'images' => $faker->imageUrl(),
        		'descriptions' => $faker->text,
        		'lyrics' =>  $faker->text,
        		'download_link' => $faker->imageUrl(),
        		'album_id' => '1',
        		'tags' =>  'ashjad,ádjkd,cc',
        		'youtube_id' => $faker->youtubeEmbedUri()
        	],



        ];
        foreach ($items as $item) {
        	SongList::create($item);
        }
    }
}
