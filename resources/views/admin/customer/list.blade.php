@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Gửi mail tới khách hàng</h1>
@stop

@section('content')
    <div class="row">
        <form action="{{ route('be.send_mail_cus') }}" method="POST">
            {{ csrf_field() }}
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Nội dung email</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                            <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Tiêu đề</label>
                                        <input class="form-control {{ $errors->has('title') ? 'has-error' : '' }}"
                                               name="title"
                                               value="{{ old('title') }}">
                                        @if ($errors->has('title'))
                                            <span class="help-block text-danger"><strong>{{ $errors->first('title') }}</strong></span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Mô tả</label>
                                        <textarea id="my-editor"
                                                  class="form-control {{ $errors->has('contents') ? 'has-error' : '' }}"
                                                  name="content">{!!  old('content')  !!}</textarea>
                                        @if ($errors->has('content'))
                                            <span class="help-block text-danger"><strong>{{ $errors->first('content') }}</strong></span>
                                        @endif
                                    </div>
                            </div>
                        </div>
                </div>
                <!-- /.box-body -->
            </div>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Người nhận</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <a onclick="toogleSelectCustEmail()" class="form-control btn btn-block btn-primary">Chọn tất cả</a>
                                @if ($errors->has('emails'))
                                    <span class="help-block text-danger"><strong>{{ $errors->first('emails') }}</strong></span>
                                @endif
                            </div>

                            <div class="col-md-12" style="margin-top: 20px" id="list-cust-email">
                                @foreach($customers as $cus)
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="cust-{{ $cus->id }}"
                                                   name="emails[]" value="{{ $cus->email  }}">
                                            <label for="cust-{{ $cus->id }}" class="custom-control-label">{{ $cus->full_name }} - {{ $cus->email }}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="form-control btn btn-block btn-success">Gửi</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        </form>
    </div>
@stop
@section('js')
    <script>
        $('#lmflogo').filemanager('image');
        CKEDITOR.replace('my-editor', options);
        function toogleSelectCustEmail() {
            var checkBoxes = $("#list-cust-email input[name=emails\\[\\]]");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        }
    </script>
@stop
