<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create( [
            'id'=>1,
            'role'=>0,
            'name'=>'Hồ Xuân Dũng',
            'email'=>'xuandung38@gmail.com',
            'email_verified_at'=>NULL,
            'password'=> Hash::make('12345678'),
            'remember_token'=>NULL,
            'created_at'=>'2019-07-30 04:31:47',
            'updated_at'=>'2019-07-30 04:31:47'
        ] );
        
        \App\User::create( [
            'id'=>2,
            'role'=>0,
            'name'=>'Sonbeat21',
            'email'=>'sonbeat21@gmail.com',
            'email_verified_at'=>NULL,
            'password'=> Hash::make('12345678'),
            'remember_token'=>NULL,
            'created_at'=>'2019-07-30 04:31:47',
            'updated_at'=>'2019-07-30 04:31:47'
        ] );
    }
}
