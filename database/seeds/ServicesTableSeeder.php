<?php

use Illuminate\Database\Seeder;
use App\Models\Services;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
        	[
	         'title'  => 'Ca nhac A', 
	         'slug'  => 'ca-nhac-a', 
	         'images' => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg', 
	         'descriptions' => 'mo ta', 
	         'tags'  => 'nhatminh, minh',
	         'soundclouds' => 'https://soundcloud.com/trippie-hippie-2/sets/album?fbclid=IwAR0re3SoMWiVzn9YDzHAnFLlz4KQczsvkCYvylO2ZEsv7WLY_RTkyUvcMvE'
	     	],
	     	[
	         'title'  => 'Ca nhac B', 
	         'slug'  => 'ca-nhac-b', 
	         'images' => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg', 
	         'descriptions' => 'mo ta', 
	         'tags'  => 'nhatminh, minh',
	         'soundclouds' => 'https://soundcloud.com/trippie-hippie-2/sets/album?fbclid=IwAR0re3SoMWiVzn9YDzHAnFLlz4KQczsvkCYvylO2ZEsv7WLY_RTkyUvcMvE'
	     	],
	     	[
	         'title'  => 'Ca nhac C', 
	         'slug'  => 'ca-nhac-c', 
	         'images' => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg', 
	         'descriptions' => 'mo ta', 
	         'tags'  => 'nhatminh, minh',
	         'soundclouds' => 'https://soundcloud.com/trippie-hippie-2/sets/album?fbclid=IwAR0re3SoMWiVzn9YDzHAnFLlz4KQczsvkCYvylO2ZEsv7WLY_RTkyUvcMvE'
	     	],
	     	[
	         'title'  => 'Ca nhac D', 
	         'slug'  => 'ca-nhac-d', 
	         'images' => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg', 
	         'descriptions' => 'mo ta', 
	         'tags'  => 'nhatminh, minh',
	         'soundclouds' => 'https://soundcloud.com/trippie-hippie-2/sets/album?fbclid=IwAR0re3SoMWiVzn9YDzHAnFLlz4KQczsvkCYvylO2ZEsv7WLY_RTkyUvcMvE'
	     	],
	     	[
	         'title'  => 'Ca nhac F', 
	         'slug'  => 'ca-nhac-f', 
	         'images' => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg', 
	         'descriptions' => 'mo ta', 
	         'tags'  => 'nhatminh, minh',
	         'soundclouds' => 'https://soundcloud.com/trippie-hippie-2/sets/album?fbclid=IwAR0re3SoMWiVzn9YDzHAnFLlz4KQczsvkCYvylO2ZEsv7WLY_RTkyUvcMvE'
	     	],
	     	[
	         'title'  => 'Ca nhac P', 
	         'slug'  => 'ca-nhac-p', 
	         'images' => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg', 
	         'descriptions' => 'mo ta', 
	         'tags'  => 'nhatminh, minh',
	         'soundclouds' => 'https://soundcloud.com/trippie-hippie-2/sets/album?fbclid=IwAR0re3SoMWiVzn9YDzHAnFLlz4KQczsvkCYvylO2ZEsv7WLY_RTkyUvcMvE'
	     	],
	     	[
	         'title'  => 'Ca nhac V', 
	         'slug'  => 'ca-nhac-v', 
	         'images' => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg', 
	         'descriptions' => 'mo ta', 
	         'tags'  => 'nhatminh, minh',
	         'soundclouds' => 'https://soundcloud.com/trippie-hippie-2/sets/album?fbclid=IwAR0re3SoMWiVzn9YDzHAnFLlz4KQczsvkCYvylO2ZEsv7WLY_RTkyUvcMvE'
	     	],
	     	[
	         'title'  => 'Ca nhac N', 
	         'slug'  => 'ca-nhac-n', 
	         'images' => 'http://dev.dzussite.leon.vm/resources/img/Teammate/1.jpg', 
	         'descriptions' => 'mo ta', 
	         'tags'  => 'nhatminh, minh',
	         'soundclouds' => 'https://soundcloud.com/trippie-hippie-2/sets/album?fbclid=IwAR0re3SoMWiVzn9YDzHAnFLlz4KQczsvkCYvylO2ZEsv7WLY_RTkyUvcMvE'
	     	],
        ];
        foreach ($items as $item) {
	         Services::create($item);
	    }
    }
}
