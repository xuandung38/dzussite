@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Kênh</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-2">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{ $chanel->avatar }}"
                         alt="User profile picture">

                    <h3 class="profile-username text-center">{{ $chanel->name }}</h3>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Email</b> <strong class="pull-right">{{ $chanel->email }}</strong>
                            <b>Bài hát đã nhận</b> <a class="pull-right">{{ $chanel->songs->count() }}</a>
                        </li>
                    </ul>

                    <a href="{{ $chanel->chanel_url }}" target="_blank" class="btn btn-primary btn-block"><b>Xem Kênh</b></a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Giới thiệu</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! $chanel->descriptions !!}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-10">
            <div class="box box-primary">
                <div class="box-body">
                    <div>
                        <table id="listdata" class="table table-bordered table-hover">
                            <thead class="thead-dark">
                            <tr>
                                <th>Mã bài</th>
                                <th>Tên bài</th>
                                <th>Link</th>
                                <th>Nghệ danh</th>
                                <th>Họ tên</th>
                                <th>Email</th>
                                <th>Số điện thoại</th>
                                <th>Credit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($chanel->songs as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td><a href="{{ route('be.song_show', $item->id) }}"> {{ $item->song_name }}</a></td>
                                    <td><a href="{{ $item->link }}" target="_blank">[Tải về]</a></td>
                                    <td>{{ $item->customer->nick_name ?? '' }}</td>
                                    <td>{{ $item->customer->full_name ?? '' }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->customer->phone ?? '' }}</td>
                                    <td>{{ $item->credit }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Mã bài</th>
                                <th>Tên bài</th>
                                <th>Link</th>
                                <th>Nghệ danh</th>
                                <th>Họ tên</th>
                                <th>Email</th>
                                <th>Số điện thoại</th>
                                <th>Credit</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $('#listdata').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'ordering': false,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@stop
