<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\ShortLinkRequest;
use App\Models\ShortLink;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShortLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $links = ShortLink::paginate(50);
        return view('admin.shortlinks.list', compact('links'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.shortlinks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShortLinkRequest $request)
    {
        ShortLink::create($request->all());
        return redirect()->route('be.shortlink')->with(['status' => 'success', 'message' => 'Thêm Link thành công']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $link = ShortLink::findorFail($id);
        return view('admin.shortlinks.edit', compact('link'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ShortLinkRequest $request, $id)
    {
        ShortLink::findorFail($id)->update($request->all());
        return redirect()->route('be.shortlink')->with(['status' => 'success', 'message' => 'Sửa Link thành công']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShortLink::findorFail($id)->delete();
        return redirect()->route('be.shortlink')->with(['status' => 'danger', 'message' => 'Xóa Link thành công']);
    }
}
