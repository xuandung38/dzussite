@extends('frontend.layouts.app')
@section('content')
<!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <!--Mới phát hành-->
                <div class="my-5">
                    <div class="container">
                        <div class="text-center">
                            <h2 class="text-uppercase title-section">{{ trans('shop.title') }}</h2>
                            <p class="mb-5 text-muted">
                                
                            </p>
                        </div>

                        <div class="row">
                        	@foreach($shop as $s)
                            <div class="col-lg-4 mb-4">
                                <div class="card shadow">
                                    <a href="" class="card-img-top">
                                        <img src="{{$s->thumbnail}}" class="img-fluid w-100">
                                    </a>
                                    <div class="card-body">
                                        <a class="h5 text-dark font-weight-bold" href="#">{{$s->title}}</a>
                                        <p>{!!$s->descriptions!!}</p>
                                        <div class="text-danger mt-2">
                                            <strong>{{$s->price}}</strong> VND
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <nav class="my-4">
                            {!!$shop->links()!!}
                        </nav>
                    </div>
                </div>

            </div>
            <!-- End of Main Content -->
@endsection