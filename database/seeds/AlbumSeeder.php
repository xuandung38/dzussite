<?php

use Illuminate\Database\Seeder;

class AlbumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\MusicAlbum::create( [
            'id'=>1,
            'title'=>'Dolor omnis qui dolo',
            'slug'=>'dolor-omnis-qui-dolo',
            'images'=>'http://127.0.0.1:8000/storage/photos/1/66612849_352941582061872_1328642613984624640_n.jpg',
            'descriptions'=>'123213213213',
            'tags'=>'Facilis rerum harum',
            'created_at'=>'2019-07-30 04:32:32',
            'updated_at'=>'2019-07-30 04:32:32'
        ] );
    }
}
