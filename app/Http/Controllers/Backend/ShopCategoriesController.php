<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopCategories;
use Illuminate\Support\Str;

class ShopCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           $shopcategories = ShopCategories::paginate(50);
        return view('admin.shopcategories.list',compact('shopcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.shopcategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shopcategorie =  new ShopCategories();
        $shopcategorie->create([
            'title' => $request->input('title'),
            'slug' => Str::slug($request->input('title'),'-'),
        ]);
        return redirect()->route('be.shopcategorie')->with(['status' => 'success','message' => 'Thêm Shop Categories thành công']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.shopcategories.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shopcategorie = ShopCategories::findorFail($id);
        return view('admin.shopcategories.edit',compact('shopcategorie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $shopcategorie = ShopCategories::findorFail($id);
        $shopcategorie->update([
            'title' => $request->input('title'),
            'slug' => Str::slug($request->input('title'),'-')
        ]);
        return redirect()->route('be.shopcategorie')->with(['status' => 'success','message' => 'Sửa thông tin Shop Categories thành công']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShopCategories::findorFail($id)->delete();
        return redirect()->route('be.shopcategorie')->with(['status' => 'warning','message' => 'Xóa Shop Categories thành công']);
    }
}
