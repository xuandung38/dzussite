<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $fillable = [
        'title','slug','images','descriptions','tags', 'soundclouds'
    ];
}
