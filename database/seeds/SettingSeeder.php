<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\SettingSite::create( [
            'id'=>1,
            'title'=>'Dzus Records',
            'logo'=>'http://127.0.0.1:8000/storage/photos/shares/LOGO.png',
            'descriptions'=>'Dzus Records',
            'tags'=>'Blanditiis nobis odi',
            'address'=>'Nobis irure rem laud',
            'hotline'=>'Elit quia occaecat',
            'maps'=>'Praesentium voluptat',
            'facebook' => 'nahtminh',
            'instargram' => 'nahtminh',
            'youtube' => 'nhatminh',
            'created_at'=>'2019-07-30 04:37:49',
            'updated_at'=>'2019-07-30 04:38:03'
        ] );

    }
}
