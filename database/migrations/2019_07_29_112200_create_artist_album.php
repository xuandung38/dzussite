<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistAlbum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist_album', function (Blueprint $table) {

            $table->unsignedBigInteger('artists_id');
            $table->foreign('artists_id')->references('id')->on('artists')->onDelete('CASCADE');
            $table->unsignedBigInteger('music_album_id');
            $table->foreign('music_album_id')->references('id')->on('music_albums')->onDelete('CASCADE');
            $table->primary([
                'music_album_id',
                'artists_id'
            ]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artist_album');
    }
}
