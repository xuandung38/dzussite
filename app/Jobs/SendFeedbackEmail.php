<?php

namespace App\Jobs;

use App\Mail\FeedbackMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendFeedbackEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $song;

    /**
     * Create a new job instance.
     *
     * @param $song
     */
    public function __construct($song)
    {
        $this->song = $song;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->song)
            ->send(new FeedbackMail($this->song));
    }
}
