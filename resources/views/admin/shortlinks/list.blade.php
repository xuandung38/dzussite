@extends('adminlte::page')
@section('title', 'ShortLink')
@section('content_header')
    <h1>ShortLink</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-success" href="{{ route('be.shortlink.create') }}"> Thêm Link</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="listdata" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Link</th>
                            <th>Link Gốc</th>
                            <th>Visited</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($links as $key => $item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td><a href="{{ route('home.shortlink', $item->code) }}" target="_blank">{{ route('home.shortlink', $item->code) }}</a></td>
                                <td>{{ $item->link }}</td>
                                <td>{{ $item->visited }}</td>
                                <td>{{ $item->status }}</td>
                                <td>
                                    <a class="btn btn-success" href="{{ route('be.shortlink.edit', $item->id) }}"> Sửa </a>
                                    <a class="btn btn-danger" href="{{ route('be.shortlink.delete', $item->id) }}"> Xóa </a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Link</th>
                            <th>Link Gốc</th>
                            <th>Visited</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        $(function () {
            $('#listdata').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@stop

