<?php

namespace App\Models;

use App\Models\SongReceived;
use Illuminate\Database\Eloquent\Model;

class MusicChanel extends Model
{
    protected $table = 'music_chanels';

    protected $guarded = [];

    public function songs()
    {
        return $this->hasMany(SongReceived::class, 'chanel_id','id');
    }
}
