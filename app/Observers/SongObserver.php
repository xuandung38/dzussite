<?php

namespace App\Observers;

use App\Jobs\SendFeedbackEmail;
use App\Jobs\SendMailSongReceived;
use App\Models\SongReceived;
use App\Models\MusicChanel;

class SongObserver
{

    /**
     * Handle the user "created" event.
     *
     * @param SongReceived $song
     * @return void
     */
    public function created(SongReceived $song)
    {
        $adminMail =  MusicChanel::find($song->chanel_id)->email;
        SendMailSongReceived::dispatch($song, $adminMail)->onQueue('default');
        SendFeedbackEmail::dispatch($song)->onQueue('default');
    }
}
