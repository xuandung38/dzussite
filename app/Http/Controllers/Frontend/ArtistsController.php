<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Artists;

class ArtistController extends Controller
{
    public function index()
    {
    	$artists = Artists::paginate(6);
    	return view('frontend.artist.index', ['artists' => $artists]);
    }

    public function detailArtist($slug)
    {
    	$detail = Artists::where('slug', $slug)->first();
    	$list_song = $detail->songs()->paginate(6);
    	return view('frontend.artist.detail', ['detail' => $detail, 'list_song' => $list_song]);
    }
}
