<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MusicAlbum;
use App\Models\SongList;

class Artists extends Model
{
    protected $fillable = [
        'name','slug','image','descriptions', 'image_cover','tags','facebook','instargram','youtube'
    ];

    public function albums()
    {
        return $this->belongsToMany(MusicAlbum::class,'artist_album','artist_id','album_id');
    }
    public function songs()
    {
        return $this->belongsToMany(SongList::class,'artist_songs');
    }
}
