<?php

namespace App\Providers;

use App\Models\SongReceived;
use App\Observers\SongObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Schema::defaultStringLength(191);
        // $this->app['request']->server->set('HTTPS', 'on'); //TODO delete on deploy
        SongReceived::observe(SongObserver::class);
    }
}
