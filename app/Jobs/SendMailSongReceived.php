<?php

namespace App\Jobs;

use App\Mail\NewSongMail;
use App\Models\SongReceived;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendMailSongReceived implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $song;
    protected $mail;

    /**
     * Create a new job instance.
     *
     * @param $song
     * @param $mail
     */
    public function __construct($song, $mail)
    {
        $this->song = $song;
        $this->mail = $mail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->song->email)
            ->cc($this->mail)
            ->send(new NewSongMail($this->song));
    }
}
