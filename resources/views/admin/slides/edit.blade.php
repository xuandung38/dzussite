@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Event</h1>
@stop

@section('content')
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Sửa Event</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('be.slide.edit', $slide->id) }}" method="POST">
                        @csrf @method('POST')
                        <div class="col-md-12">
                            <div class="form-group  {{ $errors->has('image') ? 'has-error' : '' }}">
                                <label>Image</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a id="lmfimage" data-input="image" class="btn btn-primary ">
                                            <i class="fa fa-picture-o"></i> Choose
                                        </a>
                                    </span>
                                    <input id="image" class="form-control" value="{{ old('image', $slide->image) }}" type="text" name="image">
                                </div>
                                @if ($errors->has('image'))
                                    <span class="help-block">
                                     <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group  {{ $errors->has('link') ? 'has-error' : '' }}">
                                <label>Link Event</label>
                                <input class="form-control" name="link" value="{{ old('link', $slide->link) }}">
                                @if ($errors->has('link'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('link') }}</strong>
                                </span>
                                @endif
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <button type="submit" name="submit" class="form-control btn btn-block btn-success">Lưu</button>
                                </span>

                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>

                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
                </div>
            </div>
            <!-- /.box -->

    </section>
@stop
@section('js')
    <script>
        $('#lmfimage').filemanager('image');
        CKEDITOR.replace('my-editor', options);
    </script>
@stop
