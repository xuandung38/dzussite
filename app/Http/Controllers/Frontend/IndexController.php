<?php

namespace App\Http\Controllers\Frontend;

use App\Models\ShortLink;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\ShopProduct;
use App\Models\Services;
use App\Models\Sliders;

class IndexController extends Controller
{
    public function index()
    {
        $sliders = Sliders::take(4)->get();
    	return view('frontend.index', ['sliders' => $sliders]);
    }

    public function services()
    {
        $services = Services::paginate(6);
        return view('frontend.service.index', ['services' => $services]);
    }

    public function shopProduct()
    {
        $shop = ShopProduct::paginate(6);
        return view('frontend.shop.index', ['shop' => $shop]);
    }

    public function detailShop($slug)
    {
        $detailshop = ShopProduct::where('slug' ,$slug);
        return view('frontend.shop.detail', ['detailshop' => $detailshop]);
    }

    public function about()
    {
        $about = About::first();
        return view('frontend.about.index', ['about' => $about]);
    }

    public function shortLink($code)
    {
        $link = ShortLink::where(['code' => $code,'status' => 'on'])->first();
        if (!$link) abort(404);
        $link->update([
            'visited' =>  ($link->visited + 1)
        ]);
        return redirect($link->link);
    }
}
