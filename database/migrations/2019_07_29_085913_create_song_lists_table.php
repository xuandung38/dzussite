<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('song_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->string('images');
            $table->text('descriptions')->nullable();
            $table->longText('lyrics')->nullable();
            $table->string('download_link')->nullable();
            $table->string('tags')->nullable();
            $table->unsignedBigInteger('album_id');
            $table->foreign('album_id')
                  ->references('id')->on('music_albums')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('song_lists');
    }
}
