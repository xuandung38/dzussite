<div style="width: 100%;
    height: 100%;
    background: #CCCCCC;
    margin:0;
    padding: 100px 0 100px 0;
    align-items: center;
    font-family: Arial, serif;
">
    <div style="
        width: 700px;
        background: rgba(255, 255, 255, 1);
        box-shadow: 0 5px 20px -5px rgba(0, 0, 0, 0.1);
        margin: 0 auto;
        padding: 10px 30px;
        border-radius: 10px;
    ">

        <div align="center">
            <img src="https://dzusrecords.com/storage/app/public/photos/1/unnamed.jpg" height="200px" width="200px">
        </div>
        <hr>
        <small>(vui lòng không trả lời thư này)</small>
        <p>Chào {{ $song->customer->full_name }}, cảm ơn bạn đã gửi bài hát  {{ $song->song_name }} đến cho {{ $song->chanel->name }}, bên mình sẽ nghe kỹ và
            kiểm tra chất lượng để có phản hồi sớm
            nhất cho bạn nhé.</p>
        <p>Nếu bạn là một nghệ sỹ mới chưa nhiều kinh nghiệm trong âm nhạc, bạn hãy mang sản phẩm của mình đến DZUS
            Records để
            được hỗ trợ mix & master, góp ý về bài hát tại:</p>
        <p>Địa chỉ: 2305 Toà C, 283 Khương Trung, Thanh Xuân, Hà Nội</p>
        <p>Hotline: <b>0911.830.222</b></p>
        <p><b>DZUS Records</b> - Hãng thu âm, phát hành, sản xuất âm nhạc hàng đầu Hà Nội.</p>
        <p>Chúc bạn 1 ngày vui vẻ.</p>
    </div>
</div>
