 <!-- Footer -->
<footer class="sticky-footer bg-white ">
    <div class="container my-auto  white-text">
        <div class="copyright text-center my-auto">
            <span >
            	<a class="white-text" href="{{$settingsite->facebook}}"><i class="fab fa-facebook-square"></i> Facebook </a>
            	<a class="white-text" href="{{$settingsite->youtube}}"><i class="fab fa-youtube-square"></i> Youtube </a>
            	<a class="white-text" href="{{$settingsite->instargram}}"><i class="fab fa-instagram"></i> Instargram </a>
            	<a class="white-text" href="{{$settingsite->spotify}}"><i class="fab fa-spotify"></i> Spotify </a><br><br>	
            </span>
              <p><span>© 2019 DZUS Records</span></p>
        </div>
    </div>
</footer>
<!-- End of Footer -->