@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nhạc đã nhận</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="listdata" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Mã bài</th>
                            <th>Kênh</th>
                            <th>Tên bài</th>
                            <th>Link</th>
                            <th>Nghệ danh</th>
                            <th>Họ tên</th>
                            <th>Email</th>
                            <th>Số điện thoại</th>
                            <th>Credit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($songs as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->chanel->name }}</td>
                                <td><a href="{{ route('be.song_show', $item->id) }}"> {{ $item->song_name }}</a></td>
                                <td><a href="{{ $item->link }}" target="_blank">[Tải về]</a></td>
                                <td>{{ $item->customer->nick_name ?? '' }}</td>
                                <td>{{ $item->customer->full_name ?? '' }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->customer->phone ?? '' }}</td>
                                <td>{{ $item->credit }}  </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Mã bài</th>
                            <th>Kênh</th>
                            <th>Tên bài</th>
                            <th>Link</th>
                            <th>Nghệ danh</th>
                            <th>Họ tên</th>
                            <th>Email</th>
                            <th>Số điện thoại</th>
                            <th>Credit</th>
                        </tr>
                        </tfoot>
                    </table>
                    {{ $songs->links() }}
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        $(function () {
            $('#listdata').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'ordering': false,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@stop
