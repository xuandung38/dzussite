@extends('frontend.layouts.app')
@section('content')
<div id="content">
    <!-- Begin Page Content -->
    <!--Event-->
    <div id="owlEvent" class="owl-carousel owl-theme">
        @foreach($sliders as $slider)
        <div class="item ">
            <a href="{{$slider->link}}"><img src="{{$slider->image}}" alt=""></a>
        </div>
        @endforeach
    </div>

</div>
@endsection