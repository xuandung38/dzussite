<?php

use Illuminate\Database\Seeder;
use App\Models\Artists;
use Faker\Factory;
use Illuminate\Support\Str;

class ArtistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
    	$items = [
	        [
	         'name'          =>   $faker->name,
	         'slug'          => Str::slug($faker->name),
	         'image'         => $faker->imageUrl(),
           'image_cover'   => $faker->imageUrl(),
	         'descriptions'  => $faker->text(),
	         'tags'          => $faker->text()
	     	 ],
          [
           'name'          =>   $faker->name,
           'slug'          => Str::slug($faker->name),
           'image'         => $faker->imageUrl(),
           'image_cover'   => $faker->imageUrl(),
           'descriptions'  => $faker->text(),
           'tags'          => $faker->text()
         ],
         [
           'name'          =>   $faker->name,
           'slug'          => Str::slug($faker->name),
           'image'         => $faker->imageUrl(),
           'image_cover'   => $faker->imageUrl(),
           'descriptions'  => $faker->text(),
           'tags'          => $faker->text()
         ],
         [
           'name'          =>   $faker->name,
           'slug'          => Str::slug($faker->name),
           'image'         => $faker->imageUrl(),
           'image_cover'   => $faker->imageUrl(),
           'descriptions'  => $faker->text(),
           'tags'          => $faker->text()
         ],

	    ];
        foreach ($items as $item) {
	         Artists::create($item);
	    }
    }
}
