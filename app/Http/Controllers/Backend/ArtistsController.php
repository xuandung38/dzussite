<?php

namespace App\Http\Controllers\Backend;

use App\Models\Artists;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class ArtistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artists = Artists::paginate(50);
        return view('admin.artists.list',compact('artists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.artists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $artist =  new Artists();
        $artist->create([
            'name' => $request->input('name'),
            'slug' => Str::slug($request->input('name'),'-'),
            'image' => $request->input('image'),
            'image_cover' => $request->input('image_cover'),
            'descriptions' => $request->input('descriptions'),
            'tags' => $request->input('tags'),
            'facebook' => $request->input('facebook'),
            'instargram' => $request->input('instargram'),
            'youtube' => $request->input('youtube')
        ]);
        return redirect()->route('be.artist')->with(['status' => 'success','message' => 'Thêm nghệ sỹ thành công']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artist = Artists::findorFail($id);
        return view('admin.artists.edit',compact('artist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $artist = Artists::findorFail($id);
        $artist->update([
            'name' => $request->input('name'),
            'slug' => Str::slug($request->input('name'),'-'),
            'image' => $request->input('image'),
            'descriptions' => $request->input('descriptions'),
            'image_cover' => $request->input('image_cover'),
            'tags' => $request->input('tags'),
            'facebook' => $request->input('facebook'),
            'instargram' => $request->input('instargram'),
            'youtube' => $request->input('youtube')
        ]);
        return redirect()->route('be.artist')->with(['status' => 'success','message' => 'Sửa thông tin nghệ sỹ thành công']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Artists::findorFail($id)->delete();
        return redirect()->route('be.artist')->with(['status' => 'warning','message' => 'Xóa nghệ sỹ thành công']);
    }
}
