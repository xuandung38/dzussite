<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class StoreMusicChanelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:music_chanels,name',
            'email' => 'required|email',
            'avatar' => 'required',
            'cover' => 'required',
            'chanel_url' => 'required',
            'descriptions' => 'required',
            'status' => 'required',
        ];
    }
}
