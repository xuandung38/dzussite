<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about = About::first();
        return view('admin.about', compact('about'));
    }


    public function update(Request $request)
    {
        $about = About::first();
        $about->update(
            $request->all()
        );
        return redirect()->route('be.about')->with(['status' => 'success', 'message' => 'Cập nhật giới thiệu thành công']);
    }

}
