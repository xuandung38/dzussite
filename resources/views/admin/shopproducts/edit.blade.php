@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Shop</h1>
@stop

@section('content')
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Sửa Shop</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('be.shop.edit', $shopproduct->id) }}" method="POST">
                        @csrf @method('POST')
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tên -Shop</label>
                                <input class="form-control {{ $errors->has('title') ? 'has-error' : '' }}" name="title" value="{{ old('title', $shopproduct->title) }}"> @if ($errors->has('title'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span> @endif
                            </div>

                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea id="my-editor" class="form-control {{ $errors->has('descriptions') ? 'has-error' : '' }}" name="descriptions">
                                {!! old('descriptions', $shopproduct->descriptions) !!}
                            </textarea>
                                @if ($errors->has('descriptions'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('descriptions') }}</strong>
                        </span> @endif
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Thumbnail</label>
                                <div class="input-group">
                                <span class="input-group-btn">
                             <a id="lmfimage" data-input="image" class="btn btn-primary ">
                               <i class="fa fa-picture-o"></i> Choose
                             </a>
                           </span>
                                    <input id="image" class="form-control" value="{{ old('thumbnail', $shopproduct->thumbnail) }}" type="text" name="thumbnail">
                                </div>
                                @if ($errors->has('thumbnail'))
                                    <span class="help-block">
                                     <strong>{{ $errors->first('thumbnail') }}</strong>
                                 </span> @endif
                            </div>
                            <div class="form-group">
                                <label>Tags SEO:</label>
                                <input class="form-control {{ $errors->has('tags') ? 'has-error' : '' }}" name="tags" value="{{ old('tags', $shopproduct->tags) }}"> @if ($errors->has('tags'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('tags') }}</strong>
                        </span> @endif
                            </div>

                            <div class="form-group">
                                <label>Price:</label>
                                <input class="form-control {{ $errors->has('price') ? 'has-error' : '' }}" name="price" value="{{ old('price', $shopproduct->price) }}"> @if ($errors->has('price'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span> @endif
                            </div>
                            <div class="form-group">
                                <label>Danh Muc</label>
                                <select class="form-control select2" name="category_id" data-placeholder="Chọn ca sỹ"
                                        style="width: 100%;">
                                   @foreach($cat as $ca)
                                       <option value="{{ $ca->id }}"> {{ $ca->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                           
                            <!-- /.form-group -->

                            <div class="form-group">
                                <button type="submit" name="submit" class="form-control btn btn-block btn-success">Lưu</button>
                                </span>

                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>

                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
                </div>
            </div>
            <!-- /.box -->

    </section>
@stop
@section('js')
    <script>
        $('#lmfimage').filemanager('image');
        CKEDITOR.replace('my-editor', options);
    </script>
@stop
