<div style="
    width: 100%;
    height: auto;
    background: #FFF;
    margin:0;
    padding: 50px;
    font-family: Arial, serif;
">
   {!! $content !!}
</div>
