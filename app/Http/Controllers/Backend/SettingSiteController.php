<?php

namespace App\Http\Controllers\Backend;

use App\Models\SettingSite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class SettingSiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $settings = SettingSite::first();
        if (is_null($settings)) {
            $settings = SettingSite::create([
                'title' => '',
                'logo'  => '',
                'descriptions' => '',
                'tags' => '',
                'address' => '',
                'hotline' => '',
                'youtube'  => '',
                'facebook'  => '',
                'instargram'  => '',
                'maps'  => '',
                'spotify' => ''
            ]);
        }
        return view('admin.settingsite', compact('settings'));
    }


    public function update(Request $request)
    {
        $setting = SettingSite::first();
        $setting->update(
            $request->all()
        );
        return redirect()->route('be.settings')->with(['status' => 'success','message' => 'Cập nhật cài đặt thành công']);
    }


}
