@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Cài đặt </h1>
@stop

@section('content')
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Cài đặt</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('be.settings') }}" method="POST">
                        @csrf
                        @method('POST')
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tiêu đề</label>
                            <input  class="form-control {{ $errors->has('title') ? 'has-error' : '' }}" name="title" value="{{ old('title',$settings->title) }}">
                            @if ($errors->has('title'))
                                <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Map:</label>
                            <input  class="form-control {{ $errors->has('maps') ? 'has-error' : '' }}" name="maps" value="{{ old('maps',$settings->maps) }}">
                            @if ($errors->has('maps'))
                                <span class="help-block">
                            <strong>{{ $errors->first('maps') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('descriptions') ? 'has-error' : '' }}">
                            <label>Mô tả</label>
                            <input  class="form-control"  name="descriptions" value="{{ old('descriptions',$settings->descriptions)  }}">
                            @if ($errors->has('descriptions'))
                                <span class="help-block">
                            <strong>{{ $errors->first('descriptions') }}</strong>
                        </span>
                            @endif
                        </div>
                         <div class="form-group  {{ $errors->has('spotify') ? 'has-error' : '' }}">
                            <label>Spotify</label>
                            <input  class="form-control" name="spotify" value="{{ old('spotify',$settings->spotify) }}">
                            @if ($errors->has('spotify'))
                                <span class="help-block">
                            <strong>{{ $errors->first('spotify') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="form-group  {{ $errors->has('logo') ? 'has-error' : '' }}">
                            <label>Logo</label>
                            <div class="input-group">
                           <span class="input-group-btn">
                             <a data-input="logo" class="btn btn-primary lmflogo">
                               <i class="fa fa-picture-o"></i> Choose
                             </a>
                           </span>
                                <input id="logo" class="form-control"  value="{{ old('logo',$settings->logo) }}" type="text" name="logo">
                            </div>
                            @if ($errors->has('logo'))
                                <span class="help-block">
                                     <strong>{{ $errors->first('logo') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <div class="form-group  {{ $errors->has('ogimage') ? 'has-error' : '' }}">
                            <label>OG image</label>
                            <div class="input-group">
                           <span class="input-group-btn">
                             <a data-input="ogimage" class="btn btn-primary lmflogo">
                               <i class="fa fa-picture-o"></i> Choose
                             </a>
                           </span>
                                <input id="ogimage" class="form-control"  value="{{ old('ogimage',$settings->ogimage) }}" type="text" name="ogimage">
                            </div>
                            @if ($errors->has('ogimage'))
                                <span class="help-block">
                                     <strong>{{ $errors->first('ogimage') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
                            <label>Tags SEO:</label>
                            <input  class="form-control" name="tags" value="{{ old('tags',$settings->tags) }}">
                            @if ($errors->has('tags'))
                                <span class="help-block">
                            <strong>{{ $errors->first('tags') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                            <label>Địa chỉ:</label>
                            <input  class="form-control" name="address" value="{{ old('address',$settings->address) }}">
                            @if ($errors->has('address'))
                                <span class="help-block">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('hotline') ? 'has-error' : '' }}">
                            <label>HOTLINE:</label>
                            <input  class="form-control" name="hotline" value="{{ old('hotline',$settings->hotline) }}">
                            @if ($errors->has('hotline'))
                                <span class="help-block">
                            <strong>{{ $errors->first('hotline') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('facebook') ? 'has-error' : '' }}">
                            <label>Facebook:</label>
                            <input  class="form-control" name="facebook" value="{{ old('facebook',$settings->facebook) }}">
                            @if ($errors->has('facebook'))
                                <span class="help-block">
                            <strong>{{ $errors->first('facebook') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('instargram') ? 'has-error' : '' }}">
                            <label>Instargram:</label>
                            <input  class="form-control" name="instargram" value="{{ old('instargram',$settings->instargram) }}">
                            @if ($errors->has('instargram'))
                                <span class="help-block">
                            <strong>{{ $errors->first('instargram') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('youtube') ? 'has-error' : '' }}">
                            <label>Youtube:</label>
                            <input  class="form-control" name="youtube" value="{{ old('youtube',$settings->youtube) }}">
                            @if ($errors->has('youtube'))
                                <span class="help-block">
                            <strong>{{ $errors->first('youtube') }}</strong>
                        </span>
                            @endif
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <button type="submit" name="submit" class="form-control btn btn-block btn-success">Lưu</button>
                            </span>

                        </div>
                    <!-- /.col -->
                    <!-- /.col -->
                </div>

                </form>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
                the plugin.
            </div>
        </div>
        <!-- /.box -->

    </section>
@stop

@section('js')
    <script>
        $('.lmflogo').filemanager('image');
        CKEDITOR.replace('my-editor', options);
    </script>
@stop
