<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\StoreMusicChanelRequest;
use App\Http\Requests\Backend\UpdateMusicChanelRequest;
use App\Models\MusicChanel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MusicChanelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chanels = MusicChanel::paginate(50);
        return view('admin.chanels.list', compact('chanels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.chanels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMusicChanelRequest $request)
    {
        MusicChanel::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'slug'  => Str::slug($request->input('name')),
            'avatar' => $request->input('avatar'),
            'cover' => $request->input('cover'),
            'chanel_url' => $request->input('chanel_url'),
            'descriptions' => $request->input('descriptions'),
            'status' => $request->input('status'),
        ]);

        return redirect()->route('be.chanel.index')->with(['status' => 'success','message' => 'Thêm chanel thành công']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chanel = MusicChanel::with(['songs' =>function($query) {
        return $query->orderByDesc('id')->take(20);
    }])->find($id);
        return view('admin.chanels.show', compact('chanel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chanel = MusicChanel::find($id);
        return view('admin.chanels.edit', compact('chanel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMusicChanelRequest $request, $id)
    {
        $chanel = MusicChanel::find($id);
        $chanel->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'slug'  => Str::slug($request->input('name')),
            'avatar' => $request->input('avatar'),
            'cover' => $request->input('cover'),
            'chanel_url' => $request->input('chanel_url'),
            'descriptions' => $request->input('descriptions'),
            'status' => $request->input('status'),
        ]);

        return redirect()->route('be.chanel.index')->with(['status' => 'success','message' => 'Cập nhật chanel thành công']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $chanel =  MusicChanel::find($request->input('chanel_id'));
        $chanel->delete();

        return redirect()->route('be.chanel.index')->with(['status' => 'danger','message' => 'Xoá chanel thành công']);
    }
}
