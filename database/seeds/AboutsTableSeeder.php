<?php

use Illuminate\Database\Seeder;
use App\Models\About;

class AboutsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
        	[
        		'title' => 'Team Member',
        		'contents' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
        	], 
        ];
        foreach ( $items as  $item) {
        	About::create($item);
        }
    }
}
