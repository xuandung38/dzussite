@extends('frontend.layouts.app')
@section('content')
    <div id="content">
        <div class="my-5">
            <div class="container">
                <div class="text-center">
                    <h2 class="text-uppercase title-section">YÊU CẦU PHÁT HÀNH NHẠC TRÊN {{ $chanel->name }}</h2>
                    <p class="mb-5 text-muted">
                        Các bạn vui lòng điền đầy đủ thông tin bài hát. <br/>
                        Lưu ý: {{ $chanel->name }} chỉ phát hành các bản nhạc mới, những bài hát đã phát hành không gửi cho chúng tôi.
                        <br>
                        Vui lòng xin bản quyền từ tác giả nếu như các bản nhạc là remix, cover. Chúng tôi sẽ không hỗ trợ những bài hát không có bản quyền.
                    </p>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="col align-self-center" >
                            <form action="{{ route('storesong') }}" method="POST">
                                {{ csrf_field() }}
                                <input name="chanel_id" type="text" class="form-control" id="chanel_id" value="{{ $chanel->id }}" hidden>
                                <div class="form-group">
                                    <label class="text-bold" for="email">Email (<span class="text-danger">*</span>)</label>
                                    <input onchange="app.autoFillCusInfo('{{ route('getcustomer') }}', this)" name="email" type="email" class="form-control" id="email" value="{{ old('email') }}">
                                    @if($errors->has('email'))
                                        <small class="form-text text-danger"> {{ $errors->first('email') }}</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="text-bold" for="full_name">Họ và Tên (<span class="text-danger">*</span>)</label>
                                    <input name="full_name" type="text" class="form-control" id="full_name" value="{{ old('full_name') }}">
                                    @if($errors->has('full_name'))
                                        <small class="form-text text-danger"> {{ $errors->first('full_name') }}</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="text-bold" for="nick_name">Nghệ danh (<span class="text-danger">*</span>)</label>
                                    <input name="nick_name" type="text" class="form-control" id="nick_name" value="{{ old('nick_name') }}">
                                    @if($errors->has('nick_name'))
                                        <small class="form-text text-danger"> {{ $errors->first('nick_name') }}</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="text-bold" for="song_name">Tên bài hát - Tên nghệ sĩ (Tên nghệ sĩ remix hoặc cover nếu có) (<span class="text-danger">*</span>)</label>
                                    <input name="song_name" type="text" class="form-control" id="song_name" value="{{ old('song_name') }}">
                                    @if($errors->has('song_name'))
                                        <small class="form-text text-danger"> {{ $errors->first('song_name') }}</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="text-bold" for="link">Link Download (<span class="text-danger">*</span>)</label>
                                    <input name="link" type="text" class="form-control" id="link" value="{{ old('link') }}">
                                    @if($errors->has('link'))
                                        <small class="form-text text-danger"> {{ $errors->first('link') }}</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="text-bold" for="phone">Số điện thoại (<span class="text-danger">*</span>)</label>
                                    <input name="phone" type="text" class="form-control" id="phone" value="{{ old('phone') }}">
                                    @if($errors->has('phone'))
                                        <small class="form-text text-danger"> {{ $errors->first('phone') }}</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="text-bold" for="lyric">Lời bài hát</label>
                                    <textarea class="form-control" name="lyric" id="lyric"></textarea>
                                    @if($errors->has('lyric'))
                                        <small class="form-text text-danger"> {{ $errors->first('lyric') }}</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="text-bold" for="credit">Thông tin nghệ sĩ và các link mạng xã hội (<span class="text-danger">*</span>)</label>
                                    <textarea class="form-control" name="credit" id="credit">{{ old('credit') }}</textarea>
                                    @if ($errors->has('credit'))
                                        <span class="text-danger">
                                        <strong>{{ $errors->first('credit') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-lg btn-primary">Gửi yêu cầu phát hành trên {{ $chanel->name }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>

@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            var i=1;
            $('#add').click(function(){
                $('#dynamic_field').append('<tr id="remove_'+i+'"><td><input type="text" name="credit['+i+'][name]" placeholder="Nhập Tên" class="form-control name_list" /></td><td><input type="text" name="credit['+i+'][info]" placeholder="Thông tin" class="form-control name_list"  /></td><td><input type="text" name="credit['+i+'][social]" placeholder="Link MXH" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">Xóa</button></td></tr>');
                i++;
            });
            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#remove_'+button_id+'').remove();
                i--;
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\Frontend\StoreSendSongRequest') !!}
@endsection
