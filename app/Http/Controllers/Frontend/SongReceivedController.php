<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\StoreSendSongRequest;
use App\Models\Customer;
use App\Models\MusicChanel;
use App\Models\SongReceived;
use Illuminate\Http\Request;

class SongReceivedController extends Controller
{
    public function index($slug)
    {
        $chanel = MusicChanel::where('slug',$slug)->first();
        return view('frontend.send_song.index',compact('chanel'));
    }

    public function getCustomer(Request $request)
    {
        $customer =  Customer::whereEmail($request->input('customer'))->first();
        return response()->json($customer);
    }

    public function storeSong(StoreSendSongRequest $request)
    {
        $customer = Customer::where('email', $request->email)->first();
        if($customer === null) {
            $customer = new Customer($request->parameters());
            $customer->save();
        }
        $song = new SongReceived($request->parameters());
        $song->save();
        return view('frontend.send_song.result');
    }
}
