$(document).ready(function() {
    $("#owlEvent").owlCarousel({
        items: 1,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true,
        autoplay: true,
        nav: false,
        navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
        autoplayTimeout: 4000,
        smartSpeed: 2000,
        animateOut: 'fadeOut',
    });
});
var app = {
  autoFillCusInfo(route, el) {
      var email = $(el).val();
      var url =  route + '?customer=' + email;
      $.ajax({url: url, success: function(result){
         if(result.full_name !== undefined) {
             $('#full_name').val(result.full_name);
             $('#nick_name').val(result.nick_name);
             $('#phone').val(result.phone);
         }
      }});
  },
};
