@extends('adminlte::page')

@section('title', 'ShortLink')

@section('content_header')
    <h1>ShortLink</h1>
@stop

@section('content')
    <section class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Thêm ShortLink</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('be.shortlink.create') }}" method="POST">
                        @csrf @method('POST')
                        <div class="col-md-6">
                            <div class="form-group  {{ $errors->has('code') ? 'has-error' : '' }}">
                                <label>Code</label>
                                <input class="form-control" name="code"
                                       value="{{ old('code') }}">
                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group  {{ $errors->has('link') ? 'has-error' : '' }}">
                                <label>Link</label>
                                <input class="form-control" name="link"
                                       value="{{ old('link') }}">
                                @if ($errors->has('link'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group  {{ $errors->has('status') ? 'has-error' : '' }}">
                                <label>Kích hoạt</label>
                                <select class="form-control select2" name="status">
                                    <option value="on" @if(old('status') === 'on') selected @endif>On</option>
                                    <option value="off" @if(old('status') === 'off') selected @endif>Off</option>
                                </select>

                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <button type="submit" name="submit" class="form-control btn btn-block btn-success">Lưu
                                </button>
                                </span>

                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>

                    </form>
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.box -->

    </section>
@stop
