@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>About</h1>
@stop

@section('content')
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Cài đặt About</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('be.about') }}" method="POST">
                        @csrf
                        @method('POST')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input class="form-control {{ $errors->has('title') ? 'has-error' : '' }}" name="title"
                                       value="{{ old('title',$about->title) }}">
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                     <strong>{{ $errors->first('title') }}</strong>
                                     </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea id="my-editor"
                                          class="form-control {{ $errors->has('contents') ? 'has-error' : '' }}"
                                          name="contents">
                                            {!!  old('contents',$about->contents)  !!}
                                 </textarea>
                                @if ($errors->has('contents'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contents') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <button type="submit" name="submit" class="form-control btn btn-block btn-success">Lưu
                                </button>
                                </span>

                            </div>
                            <!-- /.col -->
                            <!-- /.col -->
                        </div>

                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                   Cài đặt giới thiệu về Dzusrecord
                </div>
            </div>
            <!-- /.box -->

    </section>
@stop

@section('js')
    <script>
        $('#lmflogo').filemanager('image');
        CKEDITOR.replace('my-editor', options);
    </script>
@stop
