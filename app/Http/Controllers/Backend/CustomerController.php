<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\SendCustomMailRequest;
use App\Jobs\SendMutilCustomMail;
use App\Models\Customer;

class CustomerController  extends Controller
{
    public function requestSendMail()
    {
        return view('admin.customer.list', ['customers' => Customer::all()]);
    }
    public function sendMail(SendCustomMailRequest $request)
    {
        SendMutilCustomMail::dispatch($request->all());
        return view('admin.customer.result');
    }

}
