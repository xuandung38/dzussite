@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Album</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-success" href="{{ route('be.album.create') }}"> Thêm</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="listdata" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Image</th>
                            <th>Tên(s)</th>
                            <th>Mô tả(s)</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($albums as $key => $item)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td><img src="{{ $item->images }}" class="img-thumbnail" width="30px"></td>
                                <td>{{ $item->title }}</td>
                                <td>{!! $item->descriptions !!} </td>
                                <td>
                                    <a class="btn btn-success" href="{{ route('be.album.edit',$item->id) }}"> Sửa </a>
                                    <a class="btn btn-danger" href="{{ route('be.album.delete',$item->id) }}"> Xóa </a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Image</th>
                            <th>Tên(s)</th>
                            <th>Mô tả(s)</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                    {{ $albums->links() }}
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        $(function () {
            $('#listdata').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@stop
