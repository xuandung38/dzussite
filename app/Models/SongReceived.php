<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SongReceived extends Model
{
    protected $table = 'songs_received';



    protected $fillable = [
        'song_name',
        'email',
        'link',
        'credit',
        'lyric',
        'chanel_id',
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'email', 'email');
    }

    public function chanel()
    {
        return $this->belongsTo(MusicChanel::class, 'chanel_id')->withDefault();
    }
}
