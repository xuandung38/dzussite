@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Chanels</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-success" href="{{ route('be.chanel.create') }}"> Thêm</a>
                </div>
                @if (session('msg'))
                    <div class="alert {{session('status')}}">
                        {{ session('message') }}
                    </div>
              @endif
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="listdata" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên Kênh</th>
                            <th>Email Kênh</th>
                            <th>Mô tả</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($chanels as $key => $item)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{!! $item->descriptions !!} </td>
                                <td>@if($item->status == 1) Kích hoạt @else Không kích hoạt @endif</td>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('be.chanel.show',$item->id) }}"> Nhạc đang chờ </a>
                                    <a class="btn btn-success" href="{{ route('be.chanel.edit',$item->id) }}"> Sửa </a>
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#delete" data-id="{{ $item->id }}" data-name="{{ $item->name }}">Xoá</button>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Tên Kênh</th>
                            <th>Email Kênh</th>
                            <th>Mô tả</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                    {{ $chanels->links() }}
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Xác nhận xoá</h4>
                </div>
                <form action="{{route('be.chanel.destroy','1')}}" method="post">
                    @method('delete')
                    @csrf
                    <div class="modal-body">
                        <p class="text-center">
                        Bạn chắc chắn muốn xoá <span id="chanel_name"></span> ?
                        </p>
                        <input type="hidden" name="chanel_id" id="chanel_id" value="">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Không, Huỷ bỏ</button>
                        <button type="submit" class="btn btn-warning">Đồng ý, Xoá</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        $(function () {
            $('#listdata').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        });

        $('#delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var chanel_id = button.data('id')
            var chanel_name = button.data('name')
            var modal = $(this)
            modal.find('.modal-body #chanel_id').val(chanel_id);
            modal.find('.modal-body #chanel_name').html(chanel_name);
        });
    </script>
@stop
